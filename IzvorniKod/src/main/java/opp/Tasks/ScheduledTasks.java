package opp.Tasks;

import opp.domain.News;
import opp.domain.User;
import opp.service.NewsService;
import opp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class ScheduledTasks {

    @Autowired
    private UserService userService;

    @Autowired
    private NewsService newsService;

    @Scheduled(cron = "0 0 0 ? * SUN")
    public void updateEditors() {
        List<User> temp = userService.listall();
        LinkedList<User> users = new LinkedList<>();
        for (User u : temp) {
            if (!(u.isAdmin()))
                users.add(u);
        }

        int numberOfNewEditors = (int) Math.round((users.size() * 0.05));
        if (numberOfNewEditors == 0)
            numberOfNewEditors++;

        users.sort(Comparator.comparingInt(User::getRating)
                .reversed()
        );

        for (User user : users) {
            if (numberOfNewEditors > 0){
                user.setEditor(true);
                numberOfNewEditors--;
                userService.updateUser(user);
            } else {
                user.setEditor(false);
                userService.updateUser(user);
            }
        }

    }

    public TaskScheduler scheduler;

    private Runnable exampleRunnable(final News news){

        Runnable aRunnable = new Runnable(){
            public void run() {
                News vijest = newsService.fetch(news.getId());

                if (vijest.getContentEvaluation().equals("CINJENICA")) {
                    for (Long id : vijest.getCinjenicaId()) {
                        User user = userService.fetch(id);
                        user.increaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getCinjenicaISubjektivnoId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getSubjektivnoId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getLazId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                } else if (vijest.getContentEvaluation().equals("SUBJEKTIVNO")) {
                    for (Long id : vijest.getSubjektivnoId()) {
                        User user = userService.fetch(id);
                        user.increaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getCinjenicaISubjektivnoId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getCinjenicaId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getLazId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                } else if (vijest.getContentEvaluation().equals("CINJENICA_I_SUBJEKTIVNO")) {
                    for (Long id : vijest.getCinjenicaISubjektivnoId()) {
                        User user = userService.fetch(id);
                        user.increaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getCinjenicaId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getSubjektivnoId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getLazId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                } else if (vijest.getContentEvaluation().equals("LAZ")) {
                    for (Long id : vijest.getLazId()) {
                        User user = userService.fetch(id);
                        user.increaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getCinjenicaISubjektivnoId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getSubjektivnoId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getCinjenicaId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                }

                if (vijest.getQualityEvaluation().equals("DOBRA")) {
                    for (Long id : vijest.getDobraId()) {
                        User user = userService.fetch(id);
                        user.increaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getLosaId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getNeutralnaId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }

                } else if (vijest.getQualityEvaluation().equals("LOSA")) {
                    for (Long id : vijest.getLosaId()) {
                        User user = userService.fetch(id);
                        user.increaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getNeutralnaId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getDobraId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                } else if (vijest.getQualityEvaluation().equals("NEUTRALNA")) {
                    for (Long id : vijest.getNeutralnaId()) {
                        User user = userService.fetch(id);
                        user.increaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getLosaId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                    for (Long id : vijest.getDobraId()) {
                        User user = userService.fetch(id);
                        if (user.getRating() > 0)
                            user.decreaseRating(1);
                        userService.updateUser(user);
                    }
                }
            }
        };

        return aRunnable;

    }


    @Async
    public void executeTaskT(News news) {
        ScheduledExecutorService localExecutor = Executors.newSingleThreadScheduledExecutor();
        scheduler = new ConcurrentTaskScheduler(localExecutor);
        Calendar executionCalendar = Calendar.getInstance();
        executionCalendar.add(Calendar.DATE, 7);
        Date executionDate = executionCalendar.getTime();

        scheduler.schedule(exampleRunnable(news), executionDate);//today at 8 pm UTC - replace it with any timestamp in miliseconds to text
    }

}
