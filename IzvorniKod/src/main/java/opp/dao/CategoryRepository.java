package opp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import opp.domain.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    List<Category> findByFatherCategoryId(Long fatherCategoryId);

    List<Category> findByCategoryLevel(int categoryLevel);
    
    Optional<Category> findByCategoryNameAndCategoryLevel(String categoryName, int categoryLevel);
    
    Optional<Category> findByCategoryNameAndFatherCategoryId(String categoryName, Long fatherCategoryId);
}
