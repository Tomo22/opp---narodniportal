package opp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;
import opp.domain.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long>{

}
