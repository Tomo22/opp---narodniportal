package opp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;
import opp.domain.News;

public interface NewsRepository extends JpaRepository<News, Long> {

}
