package opp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;
import opp.domain.User;

public interface UserRepository extends JpaRepository<User, Long>{
	Optional<User> findByNadimak(String nadimak);
}
