package opp.domain;


import javax.persistence.*;
import opp.domain.Enumerators.*;

@Entity
public class Comment {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    private User commentator;

    private String content;

    private TypeOfComment typeOfComment;

    @OneToOne
    private News news;

    private boolean isValid;


    public Long getId() {
        return id;
    }

    public void setCommentator(User commentator) {
        this.commentator = commentator;
    }

    public User getCommentator() {
        return commentator;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setTypeOfComment(TypeOfComment typeOfComment) {
        this.typeOfComment = typeOfComment;
    }

    public TypeOfComment getTypeOfComment() {
        return typeOfComment;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}
