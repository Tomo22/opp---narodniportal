package opp.domain;


public class Enumerators {

    public enum ContentEvaluation{
        CINJENICA,
        SUBJEKTIVNO,
        CINJENICA_I_SUBJEKTIVNO,
        LAZ
    }

    public enum QualityEvaluation{
        DOBRA,
        NEUTRALNA,
        LOSA
    }

    public enum TypeOfComment{
        REGISTRIRANI,
        NEREGISTRIRANI,
        DOKAZI
    }

}
