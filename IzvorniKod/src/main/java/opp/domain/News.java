package opp.domain;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

import opp.service.CategoryService;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


@Entity
public class News {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    private User publisher;

    @Size(min=1)
    private String headline;

    @Column(columnDefinition = "text")
    @Size(min = 1)
    private String content;

    @Size(min = 1)
    private String proof;

    @OneToMany
    private Set<Comment> comments;




    private HashMap<Long, Integer> categoryFirstLevel = new HashMap<>();

    private HashMap<Long, Integer> categorySecondLevel = new HashMap<>();

    private HashMap<Long, Integer> categoryThirdLevel = new HashMap<>();


    private long CINJENICA = 0;

    private long SUBJEKTIVNO = 0;

    private long CINJENICA_I_SUBJEKTIVNO = 0;

    private long LAZ = 0;

    private long DOBRA = 0;

    private long LOSA = 0;

    private long NEUTRALNA = 0;

    private long categoryFirstLevelEvaluation = 0;

    private long categorySecondLevelEvaluation = 0;

    private long categoryThirdLevelEvaluation = 0;
    
    private String categoryFirstLevelEvaluationName;
    
    private String categorySecondLevelEvaluationName;
    
    private String categoryThirdLevelEvaluationName;
    
    private String contentEvaluation = "CINJENICA_I_SUBJEKTIVNO" ;

    private String qualityEvaluation = "NEUTRALNA";

    private LinkedList<Long> contentEvaluatedId = new LinkedList<>();

    private LinkedList<Long> qualityEvaluatedId = new LinkedList<>();

    private LinkedList<Long> categoryFirstLevelEvaluatedId = new LinkedList<>();

    private LinkedList<Long> categorySecondLevelEvaluatedId = new LinkedList<>();

    private LinkedList<Long> categoryThirdLevelEvaluatedId = new LinkedList<>();

    private LinkedList<Long> cinjenicaId = new LinkedList<>();

    private LinkedList<Long> subjektivnoId= new LinkedList<>();

    private LinkedList<Long> cinjenicaISubjektivnoId = new LinkedList<>();

    private LinkedList<Long> lazId = new LinkedList<>();

    private LinkedList<Long> dobraId = new LinkedList<>();

    private LinkedList<Long> losaId = new LinkedList<>();

    private LinkedList<Long> neutralnaId = new LinkedList<>();

    private HashMap<Long, Long> evaluatedCategoryFirstLevel = new HashMap<>();

    private HashMap<Long, Long> evaluatedCategorySecondLevel = new HashMap<>();

    private HashMap<Long, Long> evaluatedCategoryThirdLevel = new HashMap<>();

    private HashMap<Long, Long> secondLevelCategoryInfo = new HashMap<>();

    private HashMap<Long, Long> thirdLevelCategoryInfo = new HashMap<>();

    private boolean fakeNews = false;

    private boolean closedProofCommentSection = false;

    private boolean newsProofValid;




    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public Long getId() {
        return id;
    }

    public User getPublisher() {
        return publisher;
    }

    public void setPublisher(User publisher) {
        this.publisher = publisher;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getProof() {
        return proof;
    }

    public void setProof(String proof) {
        this.proof = proof;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void addComments(Comment comment) {
        this.comments.add(comment);
    }


    public Long getCategoryFirstLevelEvaluation() {
        return categoryFirstLevelEvaluation;

    }

    public Long getCategorySecondLevelEvaluation(){
        return categorySecondLevelEvaluation;
    }

    public Long getCategoryThirdLevelEvaluation(){
        return categoryThirdLevelEvaluation;
    }
    
    public String getCategoryFirstLevelEvaluationName() {
        return categoryFirstLevelEvaluationName;

    }

    public String getCategorySecondLevelEvaluationName(){
        return categorySecondLevelEvaluationName;
    }

    public String getCategoryThirdLevelEvaluationName(){
        return categoryThirdLevelEvaluationName;
    }




    public void setCategoryFirstLevelEvaluation(Long categoryId, Integer rating){
        int max=0;
        long maxId=categoryFirstLevelEvaluation;
        int addedrating=0;



        if(categoryFirstLevel.containsKey(categoryId)) {
            addedrating=categoryFirstLevel.get(categoryId);
        }
        addedrating+=rating;
        categoryFirstLevel.put(categoryId, addedrating);
        // tu ide provjera da li postoji kategorija sa tim id-om i da li je prave razine, i za 2. i 3. razinu da li je father category Id isti kao i id kategorije za koju je glasao iznad

        for (Map.Entry<Long, Integer> entry : categoryFirstLevel.entrySet()) {
            if(entry.getValue()>max){
                maxId=entry.getKey();
                max=entry.getValue();
            }
        }
        categoryFirstLevelEvaluation = maxId;

    }

    // u setCategorySecond i third level evaluation moraš na drugom djelu provjeravat da kategorija koja se seta paše nadkategoriji koja je sigurno

    public void setCategorySecondLevelEvaluation(Long categoryId, Integer rating, Long fatherCategoryId){
        int max=0;
        long maxId=categorySecondLevelEvaluation;
        int addedrating=0;

        if(categorySecondLevel.containsKey(categoryId)) {
            addedrating=categorySecondLevel.get(categoryId);
        }
        addedrating+=rating;
        if(!secondLevelCategoryInfo.containsKey(categoryId)) {
            secondLevelCategoryInfo.put(categoryId, fatherCategoryId);
        }
        categorySecondLevel.put(categoryId, addedrating);
        // tu ide provjera da li postoji kategorija sa tim id-om i da li je prave razine, i za 2. i 3. razinu da li je father category Id isti kao i id kategorije za koju je glasao iznad

        for (Map.Entry<Long, Integer> entry : categorySecondLevel.entrySet()) {
            if(entry.getValue()>max && secondLevelCategoryInfo.get(entry.getKey())==categoryFirstLevelEvaluation){
                maxId=entry.getKey();
                max=entry.getValue();
            }
        }
        categorySecondLevelEvaluation = maxId;

    }


    public void setCategoryThirdLevelEvaluation(Long categoryId, Integer rating, Long fatherCategoryId){
        int max=0;
        long maxId=categoryThirdLevelEvaluation;
        int addedrating=0;

        if(categoryThirdLevel.containsKey(categoryId)) {
            addedrating=categoryThirdLevel.get(categoryId);
        }
        addedrating+=rating;
        if(!thirdLevelCategoryInfo.containsKey(categoryId)) {
            thirdLevelCategoryInfo.put(categoryId, fatherCategoryId);
        }
        categoryThirdLevel.put(categoryId, addedrating);
        // tu ide provjera da li postoji kategorija sa tim id-om i da li je prave razine, i za 2. i 3. razinu da li je father category Id isti kao i id kategorije za koju je glasao iznad

        for (Map.Entry<Long, Integer> entry : categoryThirdLevel.entrySet()) {
            if(entry.getValue()>max && thirdLevelCategoryInfo.get(entry.getKey())==categorySecondLevelEvaluation){
                maxId=entry.getKey();
                max=entry.getValue();
            }
        }
        categoryThirdLevelEvaluation = maxId;
    }



    public void addToCategoryFirstLevelEvaluatedId(Long id, Long categoryId) {
        categoryFirstLevelEvaluatedId.add(id);
        evaluatedCategoryFirstLevel.put(categoryId, id);

    }

    public void addToCategorySecondLevelEvaluatedId(Long id, Long categoryId) {
        categorySecondLevelEvaluatedId.add(id);
        evaluatedCategorySecondLevel.put(categoryId, id);

    }

    public void addToCategoryThirdLevelEvaluatedId(Long id, Long categoryId) {
        categoryThirdLevelEvaluatedId.add(id);
        evaluatedCategoryThirdLevel.put(categoryId, id);

    }


    public boolean checkCategoryFirstLevelEvaluatedId(Long sentId) {
        for (Long id : categoryFirstLevelEvaluatedId) {
            if (sentId.equals(id))
                return false;
        }
        return true;
    }

    public boolean checkCategorySecondLevelEvaluatedId(Long sentId) {
        for (Long id : categorySecondLevelEvaluatedId) {
            if (sentId.equals(id))
                return false;
        }
        return true;
    }

    public boolean checkCategoryThirdLevelEvaluatedId(Long sentId) {
        for (Long id : categoryThirdLevelEvaluatedId) {
            if (sentId.equals(id))
                return false;
        }
        return true;
    }




    public String getContentEvaluation() {
        return contentEvaluation;
    }



    public void setContentEvaluation(String contentEvaluation, Integer rating) {
        if (contentEvaluation.equals("CINJENICA"))
            CINJENICA = CINJENICA + rating;
        else if (contentEvaluation.equals("SUBJEKTIVNO"))
            SUBJEKTIVNO = SUBJEKTIVNO + rating;
        else if (contentEvaluation.equals("CINJENICA_I_SUBJEKTIVNO"))
            CINJENICA_I_SUBJEKTIVNO = CINJENICA_I_SUBJEKTIVNO + rating;
        else if (contentEvaluation.equals("LAZ"))
            LAZ = LAZ + rating;
        else
            throw new IllegalArgumentException("Evaluacija mora biti: CINJENICA, SUBJEKTIVNO, CINJENICA_I_SUBJEKTIVNO ili LAZ");

        if (CINJENICA > SUBJEKTIVNO && CINJENICA > CINJENICA_I_SUBJEKTIVNO && CINJENICA > LAZ)
            this.contentEvaluation = "CINJENICA";
        else if (SUBJEKTIVNO > CINJENICA && SUBJEKTIVNO > CINJENICA_I_SUBJEKTIVNO && SUBJEKTIVNO > LAZ)
            this.contentEvaluation = "SUBJEKTIVNO";
        else if (CINJENICA_I_SUBJEKTIVNO > CINJENICA && CINJENICA_I_SUBJEKTIVNO > SUBJEKTIVNO && CINJENICA_I_SUBJEKTIVNO > LAZ)
            this.contentEvaluation = "CINJENICA_I_SUBJEKTIVNO";
        else if (LAZ > CINJENICA && LAZ > CINJENICA_I_SUBJEKTIVNO && LAZ > SUBJEKTIVNO)
            this.contentEvaluation = "LAZ";
    }

    public String getQualityEvaluation() {
        return qualityEvaluation;
    }

    public void setQualityEvaluation(String qualityEvaluation, Integer rating) {
        if (qualityEvaluation.equals("DOBRA"))
            DOBRA = DOBRA + rating;
        else if (qualityEvaluation.equals("LOSA"))
            LOSA = LOSA + rating;
        else if (qualityEvaluation.equals("NEUTRALNA"))
            NEUTRALNA = NEUTRALNA + rating;
        else
            throw new IllegalArgumentException("Evaluacija mora biti: DOBRA, LOSA ili NEUTRALNA");

        if (DOBRA > LOSA && DOBRA > NEUTRALNA)
            this.qualityEvaluation = "DOBRA";
        else if (LOSA > DOBRA && LOSA > NEUTRALNA)
            this.qualityEvaluation = "LOSA";
        else if (NEUTRALNA > DOBRA && NEUTRALNA > LOSA)
            this.qualityEvaluation = "NEUTRALNA";
    }

    public void addToQualityEvaluatedId(Long id, String evaluation) {
        qualityEvaluatedId.add(id);
        if (evaluation.equals("DOBRA"))
            dobraId.add(id);
        if (evaluation.equals("LOSA"))
            losaId.add(id);
        if (evaluation.equals("NEUTRALNA"))
            neutralnaId.add(id);
    }

    public boolean checkQualityEvaluatedId(Long sentId) {
        for (Long id : qualityEvaluatedId) {
            if (sentId.equals(id))
                return false;
        }
        return true;
    }

    public boolean checkContentEvaluatedId(Long sentId) {
        for (Long id : contentEvaluatedId) {
            if (sentId.equals(id))
                return false;
        }
        return true;
    }

    public void addToContentEvaluatedId(Long id, String evaluation) {
        contentEvaluatedId.add(id);
        if (evaluation.equals("CINJENICA"))
            cinjenicaId.add(id);
        if (evaluation.equals("SUBJEKTIVNO"))
            subjektivnoId.add(id);
        if (evaluation.equals("CINJENICA_I_SUBJEKTIVNO"))
            cinjenicaISubjektivnoId.add(id);
        if (evaluation.equals("LAZ"))
            lazId.add(id);
    }

    public LinkedList<Long> getCinjenicaId() {
        return cinjenicaId;
    }

    public LinkedList<Long> getSubjektivnoId() {
        return subjektivnoId;
    }

    public LinkedList<Long> getCinjenicaISubjektivnoId() {
        return cinjenicaISubjektivnoId;
    }

    public LinkedList<Long> getLazId() {
        return lazId;
    }

    public LinkedList<Long> getDobraId() {
        return dobraId;
    }

    public LinkedList<Long> getLosaId() {
        return losaId;
    }

    public LinkedList<Long> getNeutralnaId() {
        return neutralnaId;
    }

    public HashMap<Long, Long> getEvaluatedCategoryFirstLevel(){
        return evaluatedCategoryFirstLevel;
    }

    public HashMap<Long, Long> getEvaluatedCategorySecondLevel(){
        return evaluatedCategorySecondLevel;
    }

    public HashMap<Long, Long> getEvaluatedCategoryThirdLevel(){
        return evaluatedCategoryThirdLevel;
    }


    /* par stvari u vezi evaluacije kategorije:
     *
     *  sve tri kategorije se moraju odjednom evaluirat
     *  osoba ne može više puta evaluirat, nit može poništit prijašnje evaluacije
     *
     * provjerava da li postoji kategorija sa tim id-im, i da li je točne razine, ali ne provjerava da li je to podkategorija
     * od kategorije za koju je isti user glasao. ideja je da mu neće biti ni ponuđene podkategorije od kategorije za koju
     * nije prije glasao
     *
     * no, kada se mijenja vrijednost provjera da je nova vrijednost sigurno podkategorija od trenutne vrijednosti nadkategorije,
     * zato je potrebno da svaki user odjednom glasa za sve tri, da za slučaj da se nadkategorija promijeni da se promijene
     * i sve ostale kategorije
     */

    public boolean isFakeNews() {
        return fakeNews;
    }

    public void setFakeNews(boolean fakeNews) {
        this.fakeNews = fakeNews;
    }

    public boolean isClosedProofCommentSection() {
        return closedProofCommentSection;
    }

    public void setClosedProofCommentSection(boolean closedProofCommentSection) {
        this.closedProofCommentSection = closedProofCommentSection;
    }

    public boolean isNewsProofValid() {
        return newsProofValid;
    }

    public void setNewsProofValid(boolean newsProofValid) {
        this.newsProofValid = newsProofValid;
    }
    
    public void setCategoryFirstLevelEvaluationName(String categoryName) {
    	this.categoryFirstLevelEvaluationName=categoryName;
    }
    
    public void setCategorySecondLevelEvaluationName(String categoryName) {
    	this.categorySecondLevelEvaluationName=categoryName;
    }
    
    public void setCategoryThirdLevelEvaluationName(String categoryName) {
    	this.categoryThirdLevelEvaluationName=categoryName;
    }
}
