package opp.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Notification {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    private User user;

    @NotNull
    @Size(min = 1)
    private String text;

    private Long votingForUser;

    private Long votedForNews;

    private int typeOfNotification = 0;



    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getVotingForUser() {
        return votingForUser;
    }

    public void setVotingForUser(Long votingForUser) {
        this.votingForUser = votingForUser;
    }

    public Long getVotedForNews() {
        return votedForNews;
    }

    public void setVotedForNews(Long votedForNews) {
        this.votedForNews = votedForNews;
    }

    public int getTypeOfNotification() {
        return typeOfNotification;
    }

    public void setTypeOfNotification(int typeOfNotification) {
        this.typeOfNotification = typeOfNotification;
    }
}
