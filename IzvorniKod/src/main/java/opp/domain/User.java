package opp.domain;


import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Entity
@Table(name="user")
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min = 1)
    private String ime;

    @NotNull
    @Size(min = 1)
    private String prezime;

    @Column(unique = true, nullable = false)
    @NotNull
    @Size(min = 1, max = 20)
    private String nadimak;

    @Column(unique = true, nullable = false)
    @NotNull
    @Size(min = 3)
    private String email;

    @NotNull
    @Size(min = 1, max = 20)
    private String lozinka;

    private Integer rating = 5;

    @OneToMany
    private Set<News> news;

    @OneToMany
    private Set<Comment> comments;

    @OneToMany
    private Set<Notification> notifications;

    private boolean isAdmin;

    private boolean isEditor;

    private Timestamp blockedUntil = Timestamp.valueOf("2018-09-23 10:10:10.0");

    private int votesForAdmin;

    private int votesAgainstAdmin;

    private boolean isPublic;



    public Long getId() {
        return id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getNadimak() {
        return nadimak;
    }

    public void setNadimak(String nadimak) {
        this.nadimak = nadimak;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public Integer getRating() {
        return rating;
    }

    public void increaseRating(Integer rating) {
        this.rating = this.rating + rating;
    }

    public void decreaseRating(Integer rating) {
        this.rating = this.rating - rating;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<News> getNews() {
        return news;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    public Set<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(Set<Notification> notifications) {
        this.notifications = notifications;
    }

    public boolean isAdmin(){
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
        if (isAdmin == true)
            this.isEditor = true;
    }

    public boolean isEditor() {
        return isEditor;
    }

    public void setEditor(boolean isEditor) {
        this.isEditor = isEditor;
    }

    public boolean isBlocked(){
        Calendar calendar = Calendar.getInstance();
        long newTime = calendar.getTimeInMillis();
        Timestamp time = Timestamp.valueOf("2018-09-23 10:10:10.0");
        time.setTime(newTime);
        if (this.blockedUntil.after(time))
            return true;
        else
            return false;
    }

    public void blockUser(Integer numberOfDays){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, numberOfDays);
        long newTime = calendar.getTimeInMillis();
        this.blockedUntil.setTime(newTime);
    }

    public int getVotesForAdmin() {
        return votesForAdmin;
    }

    public void setVotesForAdmin(int votesForAdmin) {
        this.votesForAdmin = votesForAdmin;
    }

    public void increaseVotesForAdmin() {
        this.votesForAdmin++;
    }

    public int getVotesAgainstAdmin() {
        return votesAgainstAdmin;
    }

    public void setVotesAgainstAdmin(int votesAgainstAdmin) {
        this.votesAgainstAdmin = votesAgainstAdmin;
    }

    public void increaseVotesAgainstAdmin() {
        this.votesAgainstAdmin++;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }
}

