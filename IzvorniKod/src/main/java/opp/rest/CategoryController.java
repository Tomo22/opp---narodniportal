package opp.rest;


import opp.domain.Category;
import opp.domain.Comment;
import opp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("")
    public List<Category> listCategories(){
        return categoryService.listall();
    }

    @GetMapping("/{id}")
    public Category getCategory(@PathVariable("id") Long id){
        return categoryService.fetch(id);
    }

    @PostMapping("")
    public ResponseEntity<Category> createCategory(@RequestBody Category category){
    	int categoryLevel=category.getCategoryLevel();
    	String categoryName=category.getCategoryName();
    	Long fatherCategoryId=category.getFatherCategoryId();
    	
        Optional<Category> categorySearch = categoryService.findByNameAndLevel(categoryName,categoryLevel );
    	if (categorySearch.isPresent() && categoryLevel==1) {
    		throw new IllegalArgumentException();
    	}
    	if(fatherCategoryId!=null) {
    		Optional<Category> categorySearchSecond = categoryService.findByNameAndFatherId(categoryName,fatherCategoryId );
    		if((categorySearchSecond.isPresent() && categoryLevel==2)||(categorySearchSecond.isPresent() && categoryLevel==3)) {
    			throw new IllegalArgumentException();
    		}
    		else {
    			Category nova = categoryService.createCategory(category);
        		return ResponseEntity.created(URI.create("/categories/" + nova.getId())).body(nova);
    		}
    	}
    	else {
    		Category nova = categoryService.createCategory(category);
    		return ResponseEntity.created(URI.create("/categories/" + nova.getId())).body(nova);
    	}
    }

    @DeleteMapping("/{id}")
    public Category deleteCategory(@PathVariable("id") Long id){
        return categoryService.deleteCategory(id);
    }

    @GetMapping("/fetch/byFather/{fatherCategoryId}")
    public List<Category> getCategoryByFatherCategoryId(@PathVariable("fatherCategoryId") Long fatherCategoryId){
        List<Category> category = categoryService.findByFatherCategoryId(fatherCategoryId);
        
        return category;
        
    }

    @GetMapping("/fetch/byLevel/{categoryLevel}")
    public List<Category> getCategoryByCategoryLevel(@PathVariable("categoryLevel") int categoryLevel){
        List<Category> category = categoryService.findByCategoryLevel(categoryLevel);
        
        return category;
        
    }


}
