package opp.rest;


import opp.domain.Comment;
import opp.domain.Enumerators;
import opp.domain.News;
import opp.domain.User;
import opp.service.CommentService;
import opp.service.NewsService;
import opp.service.RequestDeniedException;
import opp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private NewsService newsService;

    @GetMapping("")
    public List<Comment> listComment(){
        return commentService.listall();
    }

    @GetMapping("/{id}")
    public Comment getComment(@PathVariable("id") Long id){
        return commentService.fetch(id);
    }

    @PostMapping("")
    public ResponseEntity<Comment> createComment(@RequestBody Comment comment){
        Comment novi = commentService.createComment(comment);
        if (newsService.fetch(novi.getNews().getId()).isClosedProofCommentSection() && novi.getTypeOfComment().equals(Enumerators.TypeOfComment.DOKAZI))
            throw new RequestDeniedException("proof comment section is blocked");
        return ResponseEntity.created(URI.create("/comments/" + novi.getId())).body(novi);
    }

    @DeleteMapping("/{id")
    @Secured("ROLE_ADMIN")
    public Comment deleteComment(@PathVariable("id") Long id){
        return commentService.deleteComment(id);
    }

    @PostMapping("/comment_proof_validity/{commentId}/{true_false}")
    public void setCommentProofValidity(@PathVariable("commentId") Long commentId, @PathVariable("true_false") boolean value){
        Comment comment = commentService.fetch(commentId);
        if (comment.getTypeOfComment().equals(Enumerators.TypeOfComment.DOKAZI)){
            comment.setValid(value);
            commentService.updateComment(comment);
        }
        else
            throw new RequestDeniedException("comment isn't type 'DOKAZI'");
    }

}
