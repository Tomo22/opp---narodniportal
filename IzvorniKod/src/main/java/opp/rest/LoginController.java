package opp.rest;

import opp.domain.User;
import opp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;
/**
    @PostMapping("")
    public String userLoginString(@RequestBody User user){
        String nePostoji = "fail";
        if (userService.findByNadimak(user.getNadimak()).isPresent()){
            User temp = userService.findByNadimak(user.getNadimak()).get();
            if (user.getLozinka().equals(temp.getLozinka()))
                return "ok";
            else
                return nePostoji;
        } else
            return  nePostoji;
    }

    @PostMapping("")
    public Long userLoginLong(@RequestBody User user){
        Long nePostoji = new Long(0);
        if (userService.findByNadimak(user.getNadimak()).isPresent()){
            User temp = userService.findByNadimak(user.getNadimak()).get();
            if (user.getLozinka().equals(temp.getLozinka()))
                return temp.getId();
            else
                return nePostoji;
        } else
            return  nePostoji;
    }
**/
    @PostMapping("")
    public User userLoginUser(@RequestBody User user){
        if (userService.findByNadimak(user.getNadimak()).isPresent()){
            User temp = userService.findByNadimak(user.getNadimak()).get();
            if (temp.isBlocked())
                throw new IllegalArgumentException();
            if (user.getLozinka().equals(temp.getLozinka()))
                return temp;
            else
                throw new IllegalArgumentException();
        } else
            throw new IllegalArgumentException();
    }


}
