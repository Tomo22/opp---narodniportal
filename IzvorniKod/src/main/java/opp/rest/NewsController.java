package opp.rest;



import opp.Tasks.ScheduledTasks;
import opp.domain.*;
import opp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.util.*;

@RestController
@RequestMapping("/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private UserService userService;
    
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private NotificationService notificationService;

    @GetMapping("")
    public List<News> listAllNews(){
        return newsService.listall();
    }

    @GetMapping("/{id}")
    public News getNews(@PathVariable("id") Long id){
        return newsService.fetch(id);
    }

    @PostMapping("")
    public ResponseEntity<News> createNews(@RequestBody News news){
        News created = newsService.createNews(news);
        ScheduledTasks task = new ScheduledTasks();
        task.executeTaskT(news);
        return ResponseEntity.created(URI.create("/news/" + created.getId())).body(created);
    }

    @DeleteMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public News deleteNews(@PathVariable("id") Long id){
        return newsService.deleteNews(id);
    }

    @GetMapping("/comments/{id}")
    public Set<Comment> getComments(@PathVariable("id") Long id){
        HashSet<Comment> temp = new LinkedHashSet<>();
        List<Comment> comments = commentService.listall();
        for (Comment comment : comments) {
            if (comment.getNews().getId().equals(id))
                temp.add(comment);
        }
        return  temp;
    }

    @PostMapping("/evaluation/content/{userId}/{stringContentEvaluation}")
    public News contentEvaluation(@PathVariable("userId") Long userId, @PathVariable("stringContentEvaluation") String stringContentEvaluation, @RequestBody News news){
        User user = userService.fetch(userId);
        News temp = newsService.fetch(news.getId());
        if (temp.checkContentEvaluatedId(userId)){
            temp.addToContentEvaluatedId(userId, stringContentEvaluation);
            temp.setContentEvaluation(stringContentEvaluation, user.getRating());
        } else
            throw new RequestDeniedException("Already voted for content evaluation");
        return newsService.updateNews(temp);
    }

    @PostMapping("/evaluation/quality/{userId}/{stringQualityEvaluation}")
    public News qualityEvaluation(@PathVariable("userId") Long userId, @PathVariable("stringQualityEvaluation") String stringContentEvaluation, @RequestBody News news){
        News temp = newsService.fetch(news.getId());
        User user = userService.fetch(userId);
        if (temp.checkQualityEvaluatedId(userId)){
            temp.addToQualityEvaluatedId(userId, stringContentEvaluation);
            temp.setQualityEvaluation(stringContentEvaluation, user.getRating());
        } else
            throw new RequestDeniedException("Already voted for quality evaluation");
        return newsService.updateNews(temp);
    }
    
    @PostMapping("/evaluation/categoryFirstLevel/{userId}/{longCategoryFirstLevelEvaluation}")
    public News categoryFirstLevelEvaluation(@PathVariable("userId") Long userId, @PathVariable("longCategoryFirstLevelEvaluation") Long longCategoryFirstLevelEvaluation, @RequestBody News news){
        News temp = newsService.fetch(news.getId());
        User user = userService.fetch(userId);
        
        Long fetchedId;
        String fetchedName;
        Category fetchedCategory;
        
        Category sigurnoubazi;
    	Optional<Category> ubazi = categoryService.findById(longCategoryFirstLevelEvaluation);
    	if(ubazi.isPresent()) {
    		sigurnoubazi=ubazi.get();
    		if(sigurnoubazi.getCategoryLevel()==1) {
        		if (temp.checkCategoryFirstLevelEvaluatedId(userId)){
        			temp.addToCategoryFirstLevelEvaluatedId(userId, longCategoryFirstLevelEvaluation);
        			temp.setCategoryFirstLevelEvaluation(longCategoryFirstLevelEvaluation, user.getRating());
        			fetchedId=temp.getCategoryFirstLevelEvaluation();
        			fetchedCategory=categoryService.fetch(fetchedId);
        			fetchedName=fetchedCategory.getCategoryName();
        			temp.setCategoryFirstLevelEvaluationName(fetchedName);
        			
        		} else
        			throw new RequestDeniedException("Already voted for first level category evaluation");
    		}
    		else {
    			throw new IllegalArgumentException("Kategorija mora biti razine 1");
    		}
    	}
    	else {
    		throw new IllegalArgumentException("Kategorija sa tim ID-om mora postojati");
    	}
        return newsService.updateNews(temp);
    }
    
    @PostMapping("/evaluation/categorySecondLevel/{userId}/{longCategorySecondLevelEvaluation}")
    public News categorySecondLevelEvaluation(@PathVariable("userId") Long userId, @PathVariable("longCategorySecondLevelEvaluation") Long longCategorySecondLevelEvaluation, @RequestBody News news){
        News temp = newsService.fetch(news.getId());
        User user = userService.fetch(userId);
        
        Long fetchedId;
        String fetchedName;
        Category fetchedCategory;
        
        Category sigurnoubazi;
    	Optional<Category> ubazi = categoryService.findById(longCategorySecondLevelEvaluation);
        if(ubazi.isPresent()) {
    		sigurnoubazi=ubazi.get();
    		if(sigurnoubazi.getCategoryLevel()==2) {
        		if (temp.checkCategorySecondLevelEvaluatedId(userId)){
        				temp.addToCategorySecondLevelEvaluatedId(userId, longCategorySecondLevelEvaluation);
        				temp.setCategorySecondLevelEvaluation(longCategorySecondLevelEvaluation, user.getRating(),sigurnoubazi.getFatherCategoryId());
        				fetchedId=temp.getCategorySecondLevelEvaluation();
            			fetchedCategory=categoryService.fetch(fetchedId);
            			fetchedName=fetchedCategory.getCategoryName();
            			temp.setCategorySecondLevelEvaluationName(fetchedName);
        		} else
        			throw new RequestDeniedException("Already voted for second level category evaluation");
    		}
    		else {
    			throw new IllegalArgumentException("Kategorija mora biti razine 2");
    		}
    	}
    	else {
    		throw new IllegalArgumentException("Kategorija sa tim ID-om mora postojati");
    	}
        return newsService.updateNews(temp);
    }
    
    @PostMapping("/evaluation/categoryThirdLevel/{userId}/{longCategoryThirdLevelEvaluation}")
    public News categoryThirdLevelEvaluation(@PathVariable("userId") Long userId, @PathVariable("longCategoryThirdLevelEvaluation") Long longCategoryThirdLevelEvaluation, @RequestBody News news){
        News temp = newsService.fetch(news.getId());
        User user = userService.fetch(userId);
        
        Long fetchedId;
        String fetchedName;
        Category fetchedCategory;
        
        Category sigurnoubazi;
    	Optional<Category> ubazi = categoryService.findById(longCategoryThirdLevelEvaluation);
        if(ubazi.isPresent()) {
    		sigurnoubazi=ubazi.get();
    		if(sigurnoubazi.getCategoryLevel()==3) {
    			if (temp.checkCategoryThirdLevelEvaluatedId(userId)){
    					temp.addToCategoryThirdLevelEvaluatedId(userId, longCategoryThirdLevelEvaluation);
    					temp.setCategoryThirdLevelEvaluation(longCategoryThirdLevelEvaluation, user.getRating(),sigurnoubazi.getFatherCategoryId());
    					fetchedId=temp.getCategoryThirdLevelEvaluation();
            			fetchedCategory=categoryService.fetch(fetchedId);
            			fetchedName=fetchedCategory.getCategoryName();
            			temp.setCategoryThirdLevelEvaluationName(fetchedName);
    			} else
    				throw new RequestDeniedException("Already voted for third level category evaluation");
    		}
    		else {
    			throw new IllegalArgumentException("Kategorija mora biti razine 3");
    		}
    	}
    	else {
    		throw new IllegalArgumentException("Kategorija sa tim ID-om mora postojati");
    	}
        return newsService.updateNews(temp);
    }


    @PostMapping("/editor/fake_news/{newsId}")
    public void markNewsAsFake(@PathVariable("newsId") Long newsId){
        News news = newsService.fetch(newsId);
        List<User> admins = userService.listall();
        for (User u : admins) {
            if (u.isAdmin()){
                Notification notification = new Notification();
                notification.setUser(u);
                notification.setText("Slažete li se da vijest '" + news.getHeadline() + "' postane trajno označena kao lažna?");
                notification.setVotedForNews(news.getId());
                notification.setTypeOfNotification(3);
                notificationService.createNotification(notification);
            }
        }
    }

    @PostMapping("/editor/close_proof_section/{newsId}")
    public void closeProofCommentSection(@PathVariable("newsId") Long newsId){
        News news = newsService.fetch(newsId);
        news.setClosedProofCommentSection(true);
        newsService.updateNews(news);
    }

    @PostMapping("/editor/news_proof_validity/{newsId}/{true_false}")
    public void setNewsProofValidity(@PathVariable("newsId") Long newsId, @PathVariable("true_false") boolean value){
        News news = newsService.fetch(newsId);
        news.setNewsProofValid(value);
        newsService.updateNews(news);
    }

}
