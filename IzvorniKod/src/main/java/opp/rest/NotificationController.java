package opp.rest;

import opp.domain.News;
import opp.domain.Notification;
import opp.domain.User;
import opp.service.NewsService;
import opp.service.NotificationService;
import opp.service.RequestDeniedException;
import opp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/notifications")
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private UserService userService;

    @Autowired
    private NewsService newsService;

    @GetMapping("")
    public List<Notification> listAll(){
        return notificationService.listall();
    }

    @GetMapping("/{id}")
    public Notification getNotificationById(@PathVariable("id") Long id){
        return notificationService.fetch(id);
    }

    @GetMapping("/user/{userId}")
    public List<Notification> getNotificationsByUser(@PathVariable("userId") Long userId){
        LinkedList<Notification> notifications = new LinkedList<>();
        for (Notification notification : notificationService.listall()) {
            if (notification.getUser().getId().equals(userId))
                notifications.add(notification);
        }
        return notifications;
    }

    @PostMapping("")
    public ResponseEntity<Notification> createNotification(@RequestBody Notification notification){
        Notification created = notificationService.createNotification(notification);
        return ResponseEntity.created(URI.create("/notifications/" + created.getId())).body(created);
    }

    @DeleteMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public Notification deleteNotification(@PathVariable("id") Long notificationId){
        return notificationService.deleteNotification(notificationId);
    }


    @PostMapping("/mute/editor/{nadimak}")
    public String createMuteNotification(@PathVariable("nadimak") String nadimak){
        if (!userService.findByNadimak(nadimak).isPresent()) {
            throw new RequestDeniedException("user non existent");
        }

        User user = userService.findByNadimak(nadimak).get();

        if (!user.isEditor() && !user.isAdmin() && !user.isBlocked()){
            user.blockUser(2);
            userService.updateUser(user);
            return "blocked for 2 days";
        } else
            throw new RequestDeniedException("user is already muted or can't be muted");
    }

    @PostMapping("/mute/admin/{nadimak}/{numberOfDays}")
    public String createMuteNotification(@PathVariable("nadimak") String nadimak, @PathVariable("numberOfDays") Integer numberOfDays){
        if (!userService.findByNadimak(nadimak).isPresent()) {
            throw new RequestDeniedException("user non existent");
        }

        User user = userService.findByNadimak(nadimak).get();

        if (!user.isAdmin() && !user.isEditor() && !user.isBlocked()){
            user.blockUser(numberOfDays);
            userService.updateUser(user);
            return "blocked for " + numberOfDays.toString() + "days";
        } else
            throw new RequestDeniedException("user is already muted or can't be muted");
    }

    @PostMapping("/new_admin/{nadimak}")
    public void initiateVotingForNewAdmin(@PathVariable("nadimak") String nadimak){
        if (!userService.findByNadimak(nadimak).isPresent()) {
            throw new RequestDeniedException("user non existent");
        }

        User user = userService.findByNadimak(nadimak).get();

        if (user.isAdmin()){
            throw new RequestDeniedException("user is already admin");
        } else if (!user.isEditor())
            throw new RequestDeniedException("user must be editor to be proposed for admin");

        user.setVotesForAdmin(0);

        List<User> admins = userService.listall();
        for (User u : admins) {
            if (u.isAdmin()){
                Notification notification = new Notification();
                notification.setUser(u);
                notification.setText("Želite li da korisnik " + user.getNadimak() + " postane admin?");
                notification.setVotingForUser(user.getId());
                notification.setTypeOfNotification(1);
                notificationService.createNotification(notification);
            }
        }
    }

    @PostMapping("/remove_admin/{nadimak}")
    public void initiateVotingToRemoveAdmin(@PathVariable("nadimak") String nadimak){
        if (!userService.findByNadimak(nadimak).isPresent()) {
            throw new RequestDeniedException("user non existent");
        }

        User user = userService.findByNadimak(nadimak).get();

        if (!user.isAdmin()){
            throw new RequestDeniedException("user isn't admin");
        }

        user.setVotesAgainstAdmin(0);

        List<User> admins = userService.listall();
        for (User u : admins) {
            if (u.isAdmin() && !(u.getId().equals(user.getId()))){
                Notification notification = new Notification();
                notification.setUser(u);
                notification.setText("Želite li da korisnik " + user.getNadimak() + " prestane obavljati adminsku funkciju?");
                notification.setVotingForUser(user.getId());
                notification.setTypeOfNotification(2);
                notificationService.createNotification(notification);
            }
        }
    }


    @PostMapping("/voting_new_admin/{notificationId}/{true_false}")
    public void votingToAddNewAdmin(@PathVariable("notificationId") Long notificationId, @PathVariable("true_false") boolean value){
        Notification notification = notificationService.fetch(notificationId);
        Long votedForUserId = notification.getVotingForUser();
        User votedForUser = userService.fetch(votedForUserId);
        int countAdmins = 0;
        notificationService.deleteNotification(notificationId);

        for (User u : userService.listall()) {
            if (u.isAdmin())
                countAdmins = countAdmins + 1;
        }

        if (value){
            votedForUser.increaseVotesForAdmin();
            if (votedForUser.getVotesForAdmin() >= Math.round(((countAdmins + 1 ) * 3 ) / 4 ))
                votedForUser.setAdmin(true);
            userService.updateUser(votedForUser);
        }
    }



    @PostMapping("/voting_remove_admin/{notificationId}/{true_false}")
    public void votingToRemoveAdmin(@PathVariable("notificationId") Long notificationId, @PathVariable("true_false") boolean value){
        Notification notification = notificationService.fetch(notificationId);
        Long votedForUserId = notification.getVotingForUser();
        User votedForUser = userService.fetch(votedForUserId);
        notificationService.deleteNotification(notificationId);
        int countAdmins = 0;

        for (User u : userService.listall()) {
            if (u.isAdmin())
                countAdmins = countAdmins + 1;
        }

        if (value){
            votedForUser.increaseVotesAgainstAdmin();
            if (votedForUser.getVotesAgainstAdmin() >= ((countAdmins * 3) / 4) )
                votedForUser.setAdmin(false);
            userService.updateUser(votedForUser);
        }
    }


    @PostMapping("/voting_fake_news/{notificationId}/{true_false}")
    public void votingToMarkNewsAsFake(@PathVariable("notificationId") Long notificationId, @PathVariable("true_false") boolean value){
        Notification notification = notificationService.fetch(notificationId);
        Long votedForNewsId = notification.getVotedForNews();
        News news = newsService.fetch(votedForNewsId);

        if (news.isFakeNews()){
            notificationService.deleteNotification(notificationId);
            throw new RequestDeniedException("news is alredy marked as fake");
        } else{
            notificationService.deleteNotification(notificationId);
        }

        if (value){
            news.setFakeNews(true);
            newsService.updateNews(news);
        }

    }





















}
