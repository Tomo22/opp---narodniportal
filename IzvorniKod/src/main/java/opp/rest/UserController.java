package opp.rest;


import opp.domain.User;
import opp.service.RequestDeniedException;
import opp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    public List<User> listAllUsers(){
        return userService.listall();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable("id") Long id){
        return userService.fetch(id);
    }

    @GetMapping("/login/{nadimak}")
    public User getUserByNadimak(@PathVariable("nadimak") String nadimak){
        Optional<User> user = userService.findByNadimak(nadimak);
        if (user.isPresent())
            return userService.findByNadimak(nadimak).get();
        else
            throw new NullPointerException();
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable("id") Long id, @RequestBody User user){
        if (!user.getId().equals(id)){
            throw new IllegalArgumentException("Student id must remain the same");
        }
        return userService.updateUser(user);
    }

    @PostMapping("")
    public ResponseEntity<User> createUser(@RequestBody User user){
        User created = userService.createUser(user);
        return ResponseEntity.created(URI.create("/users/" + created.getId())).body(created);
    }

    @DeleteMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public User deleteUser(@PathVariable("id") Long id){
        return userService.deleteUser(id);
    }

    @PostMapping("/remove_editor/{nadimak}")
    public void removeEditor(@PathVariable("nadimak") String nadimak){
        if (!(userService.findByNadimak(nadimak).isPresent()))
            throw  new RequestDeniedException("user doesn't exist");
        User user = userService.findByNadimak(nadimak).get();
        if (user.isAdmin())
            throw new RequestDeniedException("can't perform this action on admin.");
        else if (!user.isEditor())
            throw new RequestDeniedException("user isn't editor");
        user.setEditor(false);
        userService.updateUser(user);
    }

    @PostMapping("/is_public/{nadimak}/{true_false}")
    public void setIsPublic(@PathVariable("nadimak") String nadimak, @PathVariable("true_false") boolean value){
        if (!(userService.findByNadimak(nadimak).isPresent()))
            throw  new RequestDeniedException("user doesn't exist");
        User user = userService.findByNadimak(nadimak).get();
        user.setPublic(value);
        userService.updateUser(user);
    }

}
