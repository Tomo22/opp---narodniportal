package opp.rest;

import opp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

@Service
public class UserUserDetailsService implements UserDetailsService {

    @Value("${opp.admin.password}")
    private String adminPasswordHash;

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username){
        return new User(username, adminPasswordHash, authorities(username));
    }

    private List<GrantedAuthority> authorities(String username){
        if("admin".equals(username))
            return commaSeparatedStringToAuthorityList("ROLE_ADMIN");
        opp.domain.User user = userService.findByNadimak(username).orElseThrow(() -> new UsernameNotFoundException("No user '" + username + "'"));
        if (user.isAdmin() && user.isEditor()){
            return commaSeparatedStringToAuthorityList("ROLE_ADMIN, ROLE_EDITOR, ROLE_REGISTERED");
        } else if (user.isEditor()){
            return commaSeparatedStringToAuthorityList("ROLE_EDITOR, ROLE_REGISTERED");
        } else if (!user.isAdmin() && !user.isEditor()){
            return commaSeparatedStringToAuthorityList("ROLE_REGISTERED");
        } else {
            return commaSeparatedStringToAuthorityList("ROLE_UNREGISTERED");
        }


    }

}