package opp.service;

import opp.domain.Category;

import java.util.List;
import java.util.Optional;

/**
 * Manages category database
 * @see Category
 * @author Luka Dautovic
 *
 */
public interface CategoryService {
	/**
	 * Lists all categories in the system
	 * @return a list with all categories
	 */
	List<Category> listall();

	/**
	 * Return category with given ID.
	 * @param categoryId given category ID
	 * @return category associated with given ID in the system
	 * @throws EntityMissingException if category with that ID is not found
	 * @see CategoryService#findById(long)
	 */
	Category fetch(long categoryId);

	/**
	 * Creates new category in the system.
	 * @param category object to create, with ID set to null
	 * @return created category object in the system with ID set
	 * @throws IllegalArgumentException if given category is null, or its ID is NOT null
	 * @see Category
	 */
	Category createCategory(Category category);

	/**
	 * Finds category with given ID, if exists.
	 * @param categoryId given category ID
	 * @return Optional with value of category associated with given ID in the system,
	 * or no value if one does not exist
	 * @see CategoryService#fetch
	 */
	Optional<Category> findById(long categoryId);

	/**
	 * Updates the category with that same ID.
	 * @param category object to update, with ID set
	 * @return updated category object in the system
	 * @throws IllegalArgumentException if given object is null or has null ID
	 * @throws EntityMissingException if category with given ID is not found
	 * @see CategoryService#createCategory(Category)
	 */
	Category updateCategory(Category category);

	/**
	 * Deletes one category.
	 * @param categoryId ID of category to delete from the system
	 * @return deleted data
	 * @throws EntityMissingException if category with that ID is not found
	 */
	Category deleteCategory(long categoryId);

	/**
	 * Find the category with given fatherCategoryId.
	 * @param fatherCategoryId
	 * @return Optional with value of a user with given fatherCategoryId exists in the system,
	 * no value otherwise
	 * @throws IllegalArgumentException if given fatherCategoryId is null
	 */
	List<Category> findByFatherCategoryId(Long fatherCategoryId);

	/**
	 * Find the category with given categoryLevel.
	 * @param categoryLevel
	 * @return Optional with value of a user with given categoryLevel exists in the system,
	 * no value otherwise
	 * @throws IllegalArgumentException if given categoryLevel is null
	 */
	List<Category> findByCategoryLevel(int categoryLevel);
	
	
	
	Optional<Category> findByNameAndLevel(String categoryName, int categoryLevel);
	
	Optional<Category> findByNameAndFatherId(String categoryName, Long fatherCategoryId);
}
