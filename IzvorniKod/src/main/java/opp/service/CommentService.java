package opp.service;

import opp.domain.Comment;

import java.util.List;
import java.util.Optional;

/**
 * Manages comments database
 * @see Comment
 * @author Luka Dautovic
 *
 */
public interface CommentService {
	/**
	 * Lists all comments in the system
	 * @return a list with all comments
	 */
	List<Comment> listall();
	
	/**
	 * Return comment with given ID.
	 * @param commentId given comment ID
	 * @return comment associated with given ID in the system
	 * @throws EntityMissingException if comment with that ID is not found
	 * @see CommentService#findById(long)
	 */
	Comment fetch(long commentId);
	
	/**
	   * Creates new comment in the system.
	   * @param comment object to create, with ID set to null
	   * @return created comment object in the system with ID set
	   * @throws IllegalArgumentException if given comment is null, or its ID is NOT null
	   * @see Comment
	   */
	Comment createComment(Comment comment);
	
	/**
	   * Finds comment with given ID, if exists.
	   * @param commentId given comment ID
	   * @return Optional with value of comment associated with given ID in the system,
	   * or no value if one does not exist
	   * @see CommentService#fetch
	   */
	Optional<Comment> findById(long commentId);
	
	/**
	   * Updates the comment with that same ID.
	   * @param comment object to update, with ID set
	   * @return updated comment object in the system
	   * @throws IllegalArgumentException if given object is null or has null ID
	   * @throws EntityMissingException if comment with given ID is not found
	   * @see CommentService#createComment(Comment)
	   */
	Comment updateComment(Comment comment);

	  /**
	   * Deletes one comment.
	   * @param commentId ID of comment to delete from the system
	   * @return deleted data
	   * @throws EntityMissingException if comment with that ID is not found
	   */
	Comment deleteComment(long commentId);
}
