package opp.service;

import opp.domain.News;

import java.util.List;
import java.util.Optional;

/**
 * Manages news database
 * @see News
 * @author Luka Dautovic
 *
 */
public interface NewsService {
	/**
	 * Lists all news in the system
	 * @return a list with all news
	 */
	List<News> listall();
	
	/**
	 * Return news with given ID.
	 * @param newsId given news ID
	 * @return news associated with given ID in the system
	 * @throws EntityMissingException if news with that ID is not found
	 * @see NewsService#findById(long)
	 */
	News fetch(long newsId);
	
	/**
	   * Creates new news in the system.
	   * @param news object to create, with ID set to null
	   * @return created news object in the system with ID set
	   * @throws IllegalArgumentException if given news is null, or its ID is NOT null
	   * @see News
	   */
	News createNews(News news);
	
	/**
	   * Finds news with given ID, if exists.
	   * @param newsId given news ID
	   * @return Optional with value of news associated with given ID in the system,
	   * or no value if one does not exist
	   * @see NewsService#fetch
	   */
	Optional<News> findById(long newsId);
	
	/**
	   * Updates the news with that same ID.
	   * @param news object to update, with ID set
	   * @return updated news object in the system
	   * @throws IllegalArgumentException if given object is null or has null ID
	   * @throws EntityMissingException if news with given ID is not found
	   * @see NewsService#createNews(News)
	   */
	News updateNews(News news);

	  /**
	   * Deletes one news.
	   * @param newsId ID of news to delete from the system
	   * @return deleted data
	   * @throws EntityMissingException if news with that ID is not found
	   */
	News deleteNews(long newsId);
}
