package opp.service;

import opp.domain.Notification;
import java.util.List;
import java.util.Optional;

/**
 * Manages Notification database
 * @see Notification
 * @author Fran
 */

public interface NotificationService {
    /**
     * Lists all notifications in the system
     * @return a list with all notifications
     */
    List<Notification> listall();

    /**
     * Return Notification with associated id
     * @param notificationId given notification ID
     * @return instance of Notificaiton
     */
    Notification fetch(long notificationId);

    /**
     * Creates new notification in the system.
     * @param notification object to create, with no ID present
     * @return created Notification object in the system with ID set
     * @throws IllegalArgumentException if given user is null, or its ID is NOT null
     */
    Notification createNotification(Notification notification);

    /**
     * Finds notification with given ID, if exists.
     * @param notificationId given notification ID
     * @return Optional with value of notification associated with given ID in the system,
     * or no value if one does not exist
     * @see NotificationService#fetch
     */
    Optional<Notification> findById(long notificationId);

    /**
     * Updates the notification with that same ID.
     * @param notification object to update, with ID set
     * @return updated notification object in the system
     * @throws IllegalArgumentException if given object is null or has null ID
     * @throws EntityMissingException if notification with given ID is not found
     * @see NotificationService#createNotification(Notification)
     */
    Notification updateNotification(Notification notification);

    /**
     * Deletes one notification.
     * @param notificationId ID of the notification to delete from the system
     * @return deleted data
     * @throws EntityMissingException if notifications with that ID is not found
     */
    Notification deleteNotification(long notificationId);
}
