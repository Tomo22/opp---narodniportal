package opp.service;

import opp.domain.User;
import java.util.List;
import java.util.Optional;

/**
 * Manages user database
 * @see User
 * @author Luka Dautovic
 *
 */
public interface UserService {
	/**
	 * Lists all users in the system
	 * @return a list with all users
	 */
	List<User> listall();
	
	/**
	 * Return user with given ID.
	 * @param userId given user ID
	 * @return user associated with given ID in the system
	 * @throws EntityMissingException if user with that ID is not found
	 * @see UserService#findById(long)
	 */
	User fetch(long userId);
	
	/**
	   * Creates new user in the system.
	   * @param user object to create, with ID set to null
	   * @return created user object in the system with ID set
	   * @throws IllegalArgumentException if given user is null, or its ID is NOT null
	   * @see User
	   */
	User createUser(User user);
	
	/**
	   * Finds user with given ID, if exists.
	   * @param userId given user ID
	   * @return Optional with value of user associated with given ID in the system,
	   * or no value if one does not exist
	   * @see UserService#fetch
	   */
	Optional<User> findById(long userId);
	
	/**
	   * Updates the user with that same ID.
	   * @param user object to update, with ID set
	   * @return updated user object in the system
	   * @throws IllegalArgumentException if given object is null or has null ID
	   * @throws EntityMissingException if user with given ID is not found
	   * @see UserService#createUser(User)
	   */
	User updateUser(User user);

	/**
	   * Deletes one user.
	   * @param userId ID of user to delete from the system
	   * @return deleted data
	   * @throws EntityMissingException if user with that ID is not found
	   */
	User deleteUser(long userId);


	/**
	 * Find the student with given nadimak.
	 * @param nadimak
	 * @return Optional with value of a user with given nadimak exists in the system,
	 * no value otherwise
	 * @throws IllegalArgumentException if given nadimak is null
	 */
	Optional<User> findByNadimak(String nadimak);
}
