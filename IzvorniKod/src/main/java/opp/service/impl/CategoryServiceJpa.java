package opp.service.impl;

import opp.service.CategoryService;
import opp.dao.CategoryRepository;
import opp.domain.Category;
import opp.domain.Category;
import opp.service.EntityMissingException;
import opp.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceJpa implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepo;

	@Override
	public List<Category> listall(){
		return categoryRepo.findAll();
	}

	@Override
	public Optional<Category> findById(long categoryId) {
		return categoryRepo.findById(categoryId);
	}


	@Override
	public Category fetch(long categoryId) {
		return findById(categoryId).orElseThrow(
				() -> new EntityMissingException(Category.class, categoryId)
		);
	}

	@Override
	public Category createCategory(Category category) {
		Assert.notNull(category,"Category object must be given");
		Assert.isNull(category.getId(),
				"Category ID must be null, not: " + category.getId()
		);

		return categoryRepo.save(category);
	}

	@Override
	public Category updateCategory(Category category) {
		Assert.notNull(category,"Category object must be given");
		Long categoryId = category.getId();
		if (!categoryRepo.existsById(categoryId))
			throw new EntityMissingException(Category.class, categoryId);
		return categoryRepo.save(category);
	}

	@Override
	public Category deleteCategory(long categoryId) {
		Category category = fetch(categoryId);
		categoryRepo.delete(category);
		return category;
	}

	@Override
	public List<Category> findByFatherCategoryId(Long fatherCategoryId) {
		Assert.notNull(fatherCategoryId,"Father Category Id must be given");
		return categoryRepo.findByFatherCategoryId(fatherCategoryId);
	}

	@Override
	public List<Category> findByCategoryLevel(int categoryLevel) {
		Assert.notNull(categoryLevel,"Category level must be given");
		return categoryRepo.findByCategoryLevel(categoryLevel);
	}
	
	@Override 
	public Optional<Category> findByNameAndLevel(String categoryName, int categoryLevel){
		Assert.notNull(categoryName,"Category Name must be given");
		Assert.notNull(categoryLevel,"Category Level must be given");
		
		return categoryRepo.findByCategoryNameAndCategoryLevel(categoryName, categoryLevel);
	}
	
	@Override 
	public Optional<Category> findByNameAndFatherId(String categoryName, Long fatherCategoryId){
		Assert.notNull(categoryName,"Category Name must be given");
		Assert.notNull(fatherCategoryId,"Father Id must be given");
		
		return categoryRepo.findByCategoryNameAndFatherCategoryId(categoryName, fatherCategoryId);
	}
}
