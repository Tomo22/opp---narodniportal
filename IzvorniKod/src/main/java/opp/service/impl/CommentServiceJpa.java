package opp.service.impl;

import opp.service.CommentService;
import opp.dao.CommentRepository;
import opp.domain.Comment;
import opp.domain.Comment;
import opp.service.EntityMissingException;
import opp.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceJpa implements CommentService {

	@Autowired
	private CommentRepository commentRepo;
	
	@Override
	public List<Comment> listall(){
		return commentRepo.findAll();
	}
	
	@Override
	public Optional<Comment> findById(long commentId) {
		return commentRepo.findById(commentId);
	}

	
	@Override
	public Comment fetch(long commentId) {
		return findById(commentId).orElseThrow(
	      () -> new EntityMissingException(Comment.class, commentId)
	    );
	}

	@Override
	public Comment createComment(Comment comment) {
	    Assert.notNull(comment,"Comment object must be given");
	    Assert.isNull(comment.getId(),
	      "Comment ID must be null, not: " + comment.getId()
	    );
	    
	    return commentRepo.save(comment);
	}

	@Override
	public Comment updateComment(Comment comment) {
		Assert.notNull(comment,"Comment object must be given");
	    Long commentId = comment.getId();
	    if (!commentRepo.existsById(commentId))
	      throw new EntityMissingException(Comment.class, commentId);
	    return commentRepo.save(comment);
	  }

	@Override
	public Comment deleteComment(long commentId) {
		Comment comment = fetch(commentId);
		commentRepo.delete(comment);
		return comment;
	}
}
