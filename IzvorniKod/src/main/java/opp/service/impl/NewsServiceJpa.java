package opp.service.impl;

import opp.service.NewsService;
import opp.dao.NewsRepository;
import opp.domain.News;
import opp.domain.News;
import opp.service.EntityMissingException;
import opp.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class NewsServiceJpa implements NewsService {

	@Autowired
	private NewsRepository newsRepo;
	
	@Override
	public List<News> listall(){
		return newsRepo.findAll();
	}
	
	@Override
	public Optional<News> findById(long newsId) {
		return newsRepo.findById(newsId);
	}

	
	@Override
	public News fetch(long newsId) {
		return findById(newsId).orElseThrow(
	      () -> new EntityMissingException(News.class, newsId)
	    );
	}

	@Override
	public News createNews(News news) {
	    Assert.notNull(news,"News object must be given");
	    Assert.isNull(news.getId(),
	      "News ID must be null, not: " + news.getId()
	    );
	    
	    return newsRepo.save(news);
	}

	@Override
	public News updateNews(News news) {
		Assert.notNull(news,"News object must be given");
	    Long newsId = news.getId();
	    if (!newsRepo.existsById(newsId))
	      throw new EntityMissingException(News.class, newsId);
	    return newsRepo.save(news);
	  }

	@Override
	public News deleteNews(long newsId) {
		News news = fetch(newsId);
		newsRepo.delete(news);
		return news;
	}
}
