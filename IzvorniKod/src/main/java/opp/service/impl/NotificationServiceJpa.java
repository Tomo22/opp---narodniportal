package opp.service.impl;

import opp.dao.NotificationRepository;
import opp.domain.Notification;
import opp.service.EntityMissingException;
import opp.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class NotificationServiceJpa implements NotificationService {

    @Autowired
    private NotificationRepository notificationRepository;

    @Override
    public List<Notification> listall(){
        return notificationRepository.findAll();
    }

    @Override
    public Notification fetch(long notificationId){
        return findById(notificationId).orElseThrow(
                () -> new EntityMissingException(Notification.class, notificationId)
        );
    }

    @Override
    public Notification createNotification(Notification notification){
        Assert.isNull(notification.getId(), "notification id must be null, not: " + notification.getId());
        Assert.notNull(notification, "notification must be given");
        return notificationRepository.save(notification);
    }

    @Override
    public Optional<Notification> findById(long notificationId){
        return notificationRepository.findById(notificationId);
    }

    @Override
    public Notification updateNotification(Notification notification){
        Assert.notNull(notification, "notification must be given");
        Long notificationId = notification.getId();
        if (!notificationRepository.existsById(notificationId))
            throw new EntityMissingException(Notification.class, notificationId);
        return notificationRepository.save(notification);
    }

    @Override
    public Notification deleteNotification(long notificationId){
        Notification notification = fetch(notificationId);
        notificationRepository.delete(notification);
        return notification;
    }

}
