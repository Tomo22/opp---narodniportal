package opp.service.impl;

import opp.service.UserService;
import opp.dao.UserRepository;
import opp.domain.User;
import opp.service.EntityMissingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceJpa implements UserService {

	@Autowired
	private UserRepository userRepo;
	
	@Override
	public List<User> listall(){
		return userRepo.findAll();
	}
	
	@Override
	public Optional<User> findById(long userId) {
		return userRepo.findById(userId);
	}

	
	@Override
	public User fetch(long userId) {
		return findById(userId).orElseThrow(
	      () -> new EntityMissingException(User.class, userId)
	    );
	}

	@Override
	public User createUser(User user) {
	    Assert.notNull(user,"User object must be given");
	    Assert.isNull(user.getId(),
	      "User ID must be null, not: " + user.getId()
	    );
	    
	    return userRepo.save(user);
	}

	@Override
	public User updateUser(User user) {
		Assert.notNull(user,"User object must be given");
	    Long userId = user.getId();
	    if (!userRepo.existsById(userId))
	      throw new EntityMissingException(User.class, userId);
	    return userRepo.save(user);
	  }

	@Override
	public User deleteUser(long userId) {
		User user = fetch(userId);
		userRepo.delete(user);
		return user;
	}
	
	@Override
	public Optional<User> findByNadimak(String nadimak) {
		Assert.notNull(nadimak,"Nadimak must be given");
		return userRepo.findByNadimak(nadimak);
	}
		
}
