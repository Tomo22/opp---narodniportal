import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import NewsList from "../NewsList/NewsList";
import Publish from "../Publish/Publish";
import Login from "../Login/Login";
import Header from "../Header/Header";
import './App.css';
import Register from "../Register/Register";
import NewsLink from "../NewsLink/NewsLink";

class App extends Component {

  state = {
    user:{},
      loggedIn: false
  };


   componentWillMount() {
       //console.log(JSON.parse(localStorage.getItem('user')))
       //console.log(JSON.parse(localStorage.getItem('loggedIn')))
       const logged = JSON.parse(localStorage.getItem('loggedIn'));
       const user = JSON.parse(localStorage.getItem('user'));
      //console.log(user)

       this.setState({
           user: user
       })

       this.setState({
           loggedIn : logged
       })
   }

    onLogin = (nadimak) => {
    //this.setState( { loggedIn: true });
    fetch('/users/login/'+nadimak)
        .then(data => data.json())
        .then( user =>{
            this.setState({
                user:user,
                loggedIn:true
            });

        });
    // console.log(user)




  };

  onLogout = () => {
    this.setState({
        user: {},
        loggedIn: false });
    localStorage.clear();
    window.location.reload();
  };

  render() {
      localStorage.setItem('user', JSON.stringify(this.state.user));
      localStorage.setItem('loggedIn',JSON.stringify(this.state.loggedIn))



    return (
      <BrowserRouter>
        <div>
          <Header onLogout={this.onLogout} loggedIn={this.state.loggedIn} user={this.state.user}/>
          <div className="App">
            <Switch>
              <Route path='/' exact component={NewsList}/>
              <Route path='/vijesti' exact component={NewsList}/>
              <Route path='/vijesti/objavi' component={Publish}/>
              <Route path='/login' render={(props) => <Login  {...props} onLogin={this.onLogin} />}/>
              <Route path='/register' render={(props)=> <Register {...props}/>}/>
              <Route path='/vijesti/:id' name='vijest' render={(props) => <NewsLink {...props} user={this.state.user} />}   />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
