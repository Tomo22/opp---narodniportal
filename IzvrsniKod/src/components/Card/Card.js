import React from 'react';
import './Card.css';

const Card = ({ children, title }) => (
  <div className='Card'>
    {title && <h1>{title}</h1>}
    {children}
  </div>
);

export default Card;