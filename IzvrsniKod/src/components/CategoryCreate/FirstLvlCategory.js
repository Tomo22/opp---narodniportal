import React, {Component} from 'react';
import Button from "../Button/Button";

class FirstLvlCategory extends Component{

    state ={
        categoryName: '',
        poruka: ''
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleClick = () => {

        let data = {
            categoryName: this.state.categoryName,
            categoryLevel: 1,
            fatherCategoryId: null
        }

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        };

        return fetch('/categories', options)
            .then(response => {
                console.log(response)
                if(response.ok){
                    this.setState({
                       poruka: 'Kategorija stvorena'
                    })
                }
                else{
                    this.setState( {poruka: 'Kategorija već postoji'})
                }
            })

    };

    render() {
        return(
            <div>
                <h3>Stvori kategoriju</h3>
                <label>Unesi kategoriju:</label>
                <input type='text '  name='categoryName' onChange={this.handleChange} />
                <Button onClick={this.handleClick} disabled={this.state.categoryName.length <=0}>Stvori</Button>
                {this.state.poruka}
                <br/>
                <hr/>
            </div>
        )

    }

}
export default FirstLvlCategory;
