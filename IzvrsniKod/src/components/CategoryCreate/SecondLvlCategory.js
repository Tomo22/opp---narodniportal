import React, {Component} from 'react';
import Button from "../Button/Button";

class SecondLvlCategory extends Component{
    state = {
        prva: [],
        odabrana: null,
        odabranaPrva: false,
        catName: '',
        poruka:''
    }

    componentDidMount() {
        fetch('/categories/fetch/byLevel/1')
            .then(data =>data.json())
            .then( prva => {
                this.setState( {
                    prva: prva
                })
            })
    }

    handleChange = (event) => {
        this.setState( {
            odabrana: event.target.value,
            odabranaPrva: true
        })

    };

    handleText = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleClick = () => {
        let data = {
            categoryName: this.state.catName,
            categoryLevel: 2,
            fatherCategoryId: this.state.odabrana
        }

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        };

        return fetch('/categories', options)
            .then(response => {
                console.log(response)
                if(response.ok){
                    this.setState({
                        poruka: 'Potkategorija stvorena'
                    })
                }
                else{
                    this.setState( {poruka: 'Potkategorija već postoji'})
                }
            })
    }

    render() {
        return(
            <div>
                <h4>Stvori podkategoriju 2.razine</h4>
                <label>Odaberi kategoriju:</label>
                <select onChange={this.handleChange} >
                    <option></option>
                    {
                        this.state.prva.map((first) => <option  kat={first} key={first.id} value={first.id} name={first.categoryName} >{first.categoryName}</option>)
                    }
                </select>
                <br/>
                <label>Unesi potkategoriju:</label>
                <input type='text ' disabled={!this.state.odabranaPrva} name='catName' onChange={this.handleText}/>
                <br/>
                <Button onClick={this.handleClick} disabled={(this.state.catName.length <=0) || !this.state.odabrana}>Stvori</Button>
                <br/>
                {this.state.poruka}
            </div>
        )
    }

}
export default SecondLvlCategory;
