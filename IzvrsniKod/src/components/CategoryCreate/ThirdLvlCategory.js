import React, {Component} from 'react';
import Button from "../Button/Button";

class ThirdLvlCategory extends Component{
    state= {
        prva: [],
        druga: [],
        odabrana1: null,
        odabrana2: null,
        odabranaPrva: false,
        odabranaDruga: false,
        catName: '',
        poruka: ''
    }

    componentDidMount() {
        fetch('/categories/fetch/byLevel/1')
            .then(data =>data.json())
            .then( prva => {
                this.setState( {
                    prva: prva
                })
            })

        fetch('/categories/fetch/byLevel/2')
            .then(data =>data.json())
            .then( druga => {
                this.setState( {
                    druga: druga
                })
            })
    }

    handleChangeFirst = (event) => {
        this.setState( {
            odabranaPrva: true,
            druga: this.state.druga.filter((cat) =>
                cat.fatherCategoryId.toString() === event.target.value
            )
        })
    };

    handleChange = (event) => {
        this.setState( {
            odabrana2: event.target.value,
            odabranaDruga: true
        })

    };


    handleText = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleClick = () => {
        let data = {
            categoryName: this.state.catName,
            categoryLevel: 3,
            fatherCategoryId: this.state.odabrana2
        }

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        };

        return fetch('/categories', options)
            .then(response => {
                console.log(response)
                if(response.ok){
                    this.setState({
                        poruka: 'Podkategorija stvorena'
                    })
                }
                else{
                    this.setState( {poruka: 'Podkategorija već postoji'})
                }
            })
    }


    render() {
        return (
            <div>
                <h4>Stvori podkategoriju 3. razine</h4>
                <label>Odaberi kategoriju:</label>
                <select disabled={this.state.odabranaPrva} onChange={this.handleChangeFirst} >
                    <option></option>
                    {
                        this.state.prva.map((second) => <option  kat={second} key={second.id} value={second.id} name={second.categoryName} >{second.categoryName}</option>)
                    }
                </select>
                <br/>
                <label>Odaberi podkategoriju 2. razine:</label>
                <select disabled={!(this.state.odabranaPrva && !this.state.odabranaDruga)} onChange={this.handleChange} >

                    <option></option>
                    {
                        this.state.druga.map((second) => <option  kat={second} key={second.id} value={second.id} name={second.categoryName} >{second.categoryName}</option>)
                    }
                </select>
                <br/>
                <label>Unesi podkategoriju:</label>
                <input type='text ' disabled={!(this.state.odabranaPrva && this.state.odabranaDruga)} name='catName' onChange={this.handleText}/>
                <br/>
                <Button onClick={this.handleClick} disabled={(this.state.catName.length <=0) || (this.state.odabranaPrva && !this.state.odabranaDruga)}>Stvori</Button>
                <br/>
                {this.state.poruka}

            </div>

        )
    }


}


export default ThirdLvlCategory;
