import React, {Component} from 'react';

class Comment extends Component{




    handleClick= () =>{
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: ''
        };

        let {id}=this.props.kom;
        fetch('/comments/comment_proof_validity/'+id+'/'+true, options)
            .then(response=>{
                if(response.ok){
                    window.location.reload()
                }

            })

    }

    render() {


        const {id, content, typeOfComment, commentator, valid} = this.props.kom;
        const user = JSON.parse(localStorage.getItem('user'));


        let valjanost= <label style={{color: 'red'}}>

            {valid? 'Označen kao valjan': 'Nije označen kao valjan' }
        </label>
        let oznaci=<div></div>
        if(user!== null){
            oznaci= <div>
                {(typeOfComment==='DOKAZI')&&(user.editor)?<button onClick={this.handleClick}>Označi kao valjan</button>:''}
            </div>
        }


        if( typeOfComment !== 'NEREGISTRIRANI') {
            const {ime, prezime, nadimak, email} = commentator;
            let uloga=''
            if(commentator){
                if(commentator.admin && !commentator.editor){
                    uloga= 'admin'
                }
                else if(commentator.editor && !commentator.admin){
                    uloga='urednik'

                }
                else if(commentator.admin && commentator.editor){
                    uloga='admin i urednik'
                }
                else {
                    uloga='registriran'
                }
            }

            if (typeOfComment === this.props.filter) {
                return (
                    <div>
                        <p>
                            {content}
                        </p>
                        <br/>
                        <label>{nadimak+ ' '}</label>
                        {commentator.public? <br/>:''}
                        <label>{commentator.public? (ime+' '+prezime):''}</label>
                        {commentator.public? <br/>:''}
                        <label>{commentator.public? 'email: '+email :''} </label>
                        <br/>
                        <label>{'Uloga: '+ uloga}</label>
                        <br/>
                        {typeOfComment==='DOKAZI'? valjanost: ''}
                        {valid? '': oznaci}
                        <br/>
                        <hr/>

                    </div>
                )


            }

            else{
                return(
                    <div>
                        <p></p>
                    </div>
                )
            }
        }
        else if(typeOfComment === 'NEREGISTRIRANI'){
            if(typeOfComment === this.props.filter) {
                return (
                    <div>
                        <p>
                            {content}
                        </p>
                        <br/>
                        <label>ANONIMNO</label>
                        <hr/>
                    </div>
                )
            }

            else{
                return(
                    <div>
                        <p></p>
                    </div>
                )
            }
        }

        else{
            return(
                <div>
                    <p></p>
                </div>
            )
        }

    }

}

export default Comment;
