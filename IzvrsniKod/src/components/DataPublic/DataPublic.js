import React from 'react';
import Button from "../Button/Button";

class DataPublic extends React.Component{

    state = {
        user: JSON.parse(localStorage.getItem('user')),
        message:'',
        userPublic: '',
        returnMessage: '',
        buttonClicked: false
    }

    componentDidMount() {
        fetch('/users/login/'+JSON.parse(localStorage.getItem('user')).nadimak)
            .then(data => data.json())
            .then(newUser => {

                    this.setState({
                        userPublic: newUser.public,
                        message: newUser.public? 'Vaši podatci su javni':'Vaši podatci nisu javni'
                    });
                }
            )


    }

    handleClick = (event) => {
        event.preventDefault();
        this.setState({
            returnMessage: 'Javnost podataka promijenjena',
            buttonClicked: true
        })
        const options = {
            method: 'POST',
            headers: {

                'Content-Type': 'application/json'
            },
            body: ''
        };

        if(this.state.userPublic){
            fetch('/users/is_public/'+this.state.user.nadimak+'/'+false, options)
            this.setState({
                message: 'Vaši podatci nisu javni'
            })
        }

        else{
            fetch('/users/is_public/'+this.state.user.nadimak+'/'+true, options)
            this.setState({
                message: 'Vaši podatci su javni'
            })
        }


    };






    render(){

        return(

            <div>
            {this.state.message}
            <br/>
                <br/>
            <Button disabled={this.state.buttonClicked} onClick={this.handleClick}>Promijeni</Button>
                {this.state.returnMessage}
            </div>

        )
    }

}

export default DataPublic;