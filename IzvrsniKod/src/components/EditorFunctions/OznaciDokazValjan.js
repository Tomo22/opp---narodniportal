import React, {Component} from 'react';
import Button from "../Button/Button";

class OznaciDokazValjan extends Component{

    handleClick=()=>{
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: ''
        };

        fetch('/news/editor/news_proof_validity/'+this.props.vijest.id+'/'+true, options)
            .then(response =>{
                if(response.ok){
                    window.location.reload()
                }
            })
    }

    render() {
        return(
            <Button onClick={this.handleClick}>Označi dokaz valjanim</Button>
        )
    }

}

export default OznaciDokazValjan;
