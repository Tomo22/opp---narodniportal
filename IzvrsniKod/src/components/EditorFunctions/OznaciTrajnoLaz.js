import React, {Component} from 'react';
import Button from "../Button/Button";

class OznaciTrajnoLaz extends Component{

    state={
        poruka:''
    }

    handleClick = () => {

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: ''
        };

        fetch('/news/editor/fake_news/'+this.props.vijest.id, options)
            .then(response=>{
                    if(response.ok){
                        this.setState({poruka: 'Predložili ste trajnu lažnost vijesti'})
                    }
            })

    };

    render() {
        return(
            <div>
            <Button onClick={this.handleClick}>Predloži kao trajnu laž</Button>
                <br/>
                <label>{this.state.poruka}</label>
                <br/>
                <hr/>
            </div>
        )
    }

}

export default OznaciTrajnoLaz;
