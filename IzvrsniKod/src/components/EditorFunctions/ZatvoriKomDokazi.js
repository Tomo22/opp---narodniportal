import React, {Component} from 'react';
import Button from "../Button/Button";

class ZatvoriKomDokazi extends Component{

    handleClick=()=>{
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: ''
        };

        fetch('/news/editor/close_proof_section/'+this.props.vijest.id, options)
            .then(response =>{
                if(response.ok){
                    window.location.reload()
                }
            })
    }

    render() {
        return(
            <Button onClick={this.handleClick}>Zatvori komentare s dokazima</Button>
        )
    }
}

export default ZatvoriKomDokazi;
