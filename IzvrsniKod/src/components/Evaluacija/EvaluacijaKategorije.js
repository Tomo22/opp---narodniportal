import React, {Component} from 'react';
import Button from "../Button/Button";

class EvaluacijaKategorije extends Component {
    state={
        prva:[],
        druga: [],
        treca: [],
        odabranaPrva: null,
        odabranaDruga: null,
        odabranaTreca: null,
        poruka:''
    }

    componentDidMount() {
        fetch('/categories/fetch/byLevel/1')
            .then(data =>data.json())
            .then( prva => {
                this.setState( {
                    prva: prva
                })
            })
    }


    handlePrva = (e) => {
        this.setState({odabranaPrva: e.target.value})

        if(e.target.value) {
            fetch('/categories/fetch/byFather/' + e.target.value)
                .then(data => data.json())
                .then(druga => {
                    this.setState({druga: druga})
                })
        }
        else {
            this.setState({
                druga: [],
                treca: []
             })
        }

    }

    handleDruga = (ev) => {
        this.setState({odabranaDruga: ev.target.value})

        if(ev.target.value) {
            fetch('/categories/fetch/byFather/' + ev.target.value)
                .then(data => data.json())
                .then(treca => {
                    this.setState({treca: treca})
                })
        }

        else{
            this.setState({treca:[]})
        }

    }

    handleTreca = (event) => {
        this.setState({odabranaTreca: event.target.value })

    }

    handleClick1= () =>{
        const user = JSON.parse(localStorage.getItem('user'));

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.props.vijest)
        };
        fetch('/news/evaluation/categoryFirstLevel/'+user.id+'/'+this.state.odabranaPrva,options )
            .then(response =>{
                if(response.ok){

                }
                else{
                    this.setState({poruka:'Vec si glasao za kategoriju'})
                }
            })





    }

    handleClick2= () => {
        const user = JSON.parse(localStorage.getItem('user'));

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.props.vijest)
        };

        fetch('/news/evaluation/categorySecondLevel/'+user.id+'/'+this.state.odabranaDruga,options )
            .then(response =>{

                if(response.ok){

                }
                else{
                    this.setState({poruka:'Vec si glasao za 1.podkategoriju'})
                }
            })

    }

    handleClick3= () => {
        const user = JSON.parse(localStorage.getItem('user'));

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.props.vijest)
        };

        fetch('/news/evaluation/categoryThirdLevel/'+user.id+'/'+this.state.odabranaTreca,options)
            .then(response =>{

                if(response.ok){

                }
                else{
                    this.setState({poruka:'Vec si glasao za 2.podkategoriju'})
                }
            })
    }

    render() {

        return(
            <div>
                <h4>Evalucija po kategoriji</h4>
                <label>Kategorija: </label>
                <select onChange={this.handlePrva}>
                    <option></option>
                    {
                        this.state.prva.map((first) => <option  kat={first} key={first.id} value={first.id} name={first.categoryName} >{first.categoryName}</option>)
                    }
                </select>
                <Button  disabled={!this.state.odabranaPrva } onClick={this.handleClick1}>Evaluiraj kategoriju</Button>
                <br/>
                <br/>

                <label>Kategorija 2. razine: </label>
                <select onChange={this.handleDruga}>
                    <option></option>
                    {
                        this.state.druga.map( (second) => <option kat={second} key={second.id} value={second.id} name={second.categoryName}>{second.categoryName}</option>)
                    }
                </select>

                <Button disabled={!this.state.odabranaDruga} onClick={this.handleClick2}>Evaluiraj 1. podkategoriju </Button>
                <br/>
                <br/>

                <label>Kategorija 3: razine</label>
                <select onChange={this.handleTreca}>
                    <option></option>
                    {
                        this.state.treca.map((third) => <option kat={third} key={third.id} value={third.id} name={third.categoryName}>{third.categoryName}</option>)
                    }
                </select>
                <Button disabled={!this.state.odabranaTreca} onClick={this.handleClick3}>Evaluiraj 2. podkategoriju</Button>
                <br/>
                {this.state.poruka}
                <br/>
                <hr/>
            </div>
        )
    }

}

export default EvaluacijaKategorije;
