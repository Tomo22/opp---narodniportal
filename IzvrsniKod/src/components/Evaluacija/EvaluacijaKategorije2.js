import React, {Component} from 'react';
import Button from "../Button/Button";

class EvaluacijaKategorije2 extends Component {
    state={
        prva:[],
        druga: [],
        treca: [],
        odabranaPrva: null,
        odabranaDruga: null,
        odabranaTreca: null,
        poruka:'',

    }

    componentDidMount() {
        fetch('/categories/fetch/byLevel/1')
            .then(data =>data.json())
            .then( prva => {
                this.setState( {
                    prva: prva
                })
            })
    }


    handlePrva = (e) => {
        this.setState({odabranaPrva: e.target.value})

        if(e.target.value) {
            fetch('/categories/fetch/byFather/' + e.target.value)
                .then(data => data.json())
                .then(druga => {
                    this.setState({druga: druga})
                })
        }
        else {
            this.setState({
                druga: [],
                treca: [],
                odabranaDruga: null,
                odabranaTreca: null
            })
        }

    }

    handleDruga = (ev) => {
        this.setState({odabranaDruga: ev.target.value})

        if(ev.target.value) {
            fetch('/categories/fetch/byFather/' + ev.target.value)
                .then(data => data.json())
                .then(treca => {
                    this.setState({treca: treca})
                })
        }

        else{
            this.setState({treca:[]})
        }

    }

    handleTreca = (event) => {
        this.setState({odabranaTreca: event.target.value })

    }

    handleClick1= () =>{
        const user = JSON.parse(localStorage.getItem('user'));

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.props.vijest)
        };
        fetch('/news/evaluation/categoryFirstLevel/'+user.id+'/'+this.state.odabranaPrva,options )
            .then(response =>{
                //console.log(response)
                if(response.ok){

                }
                else{
                    this.setState({poruka:'Vec si glasao za kategoriju'})
                }
            })
            .then(this.handleClick2)







    }

    handleClick2= () => {
        const user = JSON.parse(localStorage.getItem('user'));

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.props.vijest)
        };

        fetch('/news/evaluation/categorySecondLevel/'+user.id+'/'+this.state.odabranaDruga,options )
            .then(response =>{
                //console.log(response)
                if(response.ok){


                }
                else{
                    this.setState({poruka: 'Vec si glasao za kategoriju'})

                }
            })
            .then(this.handleClick3)


    }

    handleClick3= () => {
        const user = JSON.parse(localStorage.getItem('user'));

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.props.vijest)
        };

        fetch('/news/evaluation/categoryThirdLevel/'+user.id+'/'+this.state.odabranaTreca,options)
            .then(response =>{
                //console.log(response)
                if(response.ok){

                }
                else{
                    this.setState({poruka: 'Vec si glasao za kategoriju'})

                }
            })
    }

    render() {

        return(
            <div>
                <h4>Evaluacija kategorije</h4>
                <label>Kategorija </label>
                <select onChange={this.handlePrva}>
                    <option></option>
                    {
                        this.state.prva.map((first) => <option  kat={first} key={first.id} value={first.id} name={first.categoryName} >{first.categoryName}</option>)
                    }
                </select>
                <br/>

                <label>Kategorija 2. razine </label>
                <select onChange={this.handleDruga}>
                    <option></option>
                    {
                        this.state.druga.map( (second) => <option kat={second} key={second.id} value={second.id} name={second.categoryName}>{second.categoryName}</option>)
                    }
                </select>
                <br/>

                <label>Kategorija 3. razine  </label>
                <select onChange={this.handleTreca}>
                    <option></option>
                    {
                        this.state.treca.map((third) => <option kat={third} key={third.id} value={third.id} name={third.categoryName}>{third.categoryName}</option>)
                    }
                </select>
                <br/>
                <br/>
                <Button  disabled={(!this.state.odabranaPrva) || (!this.state.odabranaDruga) || (!this.state.odabranaTreca) } onClick={this.handleClick1} >Evaluiraj kategoriju</Button>
                {this.state.poruka}

                <br/>

            </div>
        )
    }

}

export default EvaluacijaKategorije2;
