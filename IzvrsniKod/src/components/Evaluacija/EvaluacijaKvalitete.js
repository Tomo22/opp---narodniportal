import React, {Component} from 'react';

class EvaluacijaKvalitete extends  Component{

    state= {
        poruka: '',

    };

    handleChange =(event) => {

        let  sadrzaj= event.target.value;


        const user = JSON.parse(localStorage.getItem('user'));
        console.log(user);



        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.props.vijest)
        };

        return fetch('/news/evaluation/quality/'+user.id+'/'+sadrzaj ,options)
            .then(response => {
                if(response.ok){

                }
                else{
                    this.setState({
                        poruka: 'Već si glasao za kvalitetu'
                    })
                }
            })
    };

    render() {
        // console.log(this.props.vijest);
        return(
            <div>
                <label>Evaluacija kvalitete:</label>
                <select value='Odaberi sadrzaj' placeholder='Odaberi sadrzaj' onChange={this.handleChange} name='sadrzaj' >
                    <option></option>
                    <option value='DOBRA'>Dobra</option>
                    <option value='NEUTRALNA'>Neutralna</option>
                    <option value='LOSA'>Loša</option>

                </select>
                <br/>

                {this.state.poruka}
            </div>
        )
    }


}

export default EvaluacijaKvalitete;
