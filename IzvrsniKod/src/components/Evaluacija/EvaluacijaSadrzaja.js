import React, {Component} from 'react';

class EvaluacijaSadrzaja extends  Component{

    state= {
        poruka: '',

    }

    handleChange =(event) => {

          let  sadrzaj= event.target.value


        const user = JSON.parse(localStorage.getItem('user'));
        console.log(user);



        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.props.vijest)
        };

        return fetch('/news/evaluation/content/'+user.id+'/'+sadrzaj ,options)
            .then(response => {
                if(response.ok){

                }
                else{
                    this.setState({
                        poruka: 'Već si glasao za sadrzaj'
                    })
                }
            })
    };

    render() {
       // console.log(this.props.vijest);
        return(
            <div>
                <hr/>
                <label>Evaluacija sadržaja:</label>
                <select value='Odaberi sadrzaj' placeholder='Odaberi sadrzaj' onChange={this.handleChange} name='sadrzaj' >
                    <option></option>
                    <option value='CINJENICA'>Činjenica</option>
                    <option value='SUBJEKTIVNO'>Subjektivno</option>
                    <option value='CINJENICA_I_SUBJEKTIVNO'>Činjenica i subjektivno</option>
                    <option value='LAZ'>Laž</option>
                </select>
                <br/>

                {this.state.poruka}
            </div>
        )
    }


}

export default EvaluacijaSadrzaja;
