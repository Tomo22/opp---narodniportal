import React, {Component} from 'react';


class CategoryFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            buttonDisable: false,
            categories: []
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        fetch('/categories/fetch/byLevel/1')
            .then(data =>data.json())
            .then( cat => {
                this.setState( {
                    categories: cat
                })
            })
    }

    handleChange = (event) => {
        this.props.onFilter(event.target.value.toString());
        this.state.buttonDisable = true;
        this.state.value = event.target.value;

    }


    render() {

        let message=<div></div>;

        if(this.state.value !== ''){
            message =
                <p>  Trenutni filter: {this.state.value}</p>
        }
        return (
            <div>
                <label>Kategorija:</label>
                <select disabled={this.state.buttonDisable} onChange={this.handleChange} >
                    <option></option>
                    <option value=''>Nesvrstano</option>
                    {
                        this.state.categories.map((second) => <option  kat={second} key={second.id} value={second.categoryName} name={second.categoryName} >{second.categoryName}</option>)
                    }
                </select>
                {message}
            </div>
        )
    }
}

export default CategoryFilter;