import React, {Component} from 'react';


class ContentFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            buttonDisable: false
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (event) => {
        this.props.onFilter(event.target.value);
        if(event.target.value === 'CINJENICA') this.state.value = 'Činjenica';
        else if(event.target.value === 'CINJENICA_I_SUBJEKTIVNO') this.state.value = 'Činjenica i subjektivno mišljenje';
        else if(event.target.value === 'SUBJEKTIVNO') this.state.value = 'Subjektivno mišljenje';
        else if(event.target.value === 'LAZ') this.state.value = 'Laž';
        this.state.buttonDisable = true;
    }


    render() {

        let message=<div></div>;

        if(this.state.value !== ''){
            message =
                <p>  Trenutni filter: {this.state.value}</p>
        }
        return (
            <div>
                <label>sadržaj:</label>
                <select  disabled={this.state.buttonDisable} value='Odaberi sadrzaj' placeholder='Odaberi sadrzaj' onChange={this.handleChange} name='sadrzaj' >
                    <option></option>
                    <option value='CINJENICA'>Činjenica</option>
                    <option value='SUBJEKTIVNO'>Subjektivno</option>
                    <option value='CINJENICA_I_SUBJEKTIVNO'>Činjenica i subjektivno</option>
                    <option value='LAZ'>Laž</option>
                </select>
                {message}
            </div>
        )
    }
}

export default ContentFilter;