import React, {Component} from 'react';


class QualityFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            buttonDisable: false,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (event) => {
        this.props.onFilter(event.target.value);
        if(event.target.value === 'DOBRA') this.state.value = 'Dobra';
        else if(event.target.value === 'NEUTRALNA') this.state.value = 'Neutralna';
        else if(event.target.value === 'LOSA') this.state.value = 'Loša';
        this.state.buttonDisable = true;
    }


    render() {

        let message=<div></div>;

        if(this.state.value !== ''){
            message = <p> Trenutni filter: {this.state.value}</p>
        }
        return (
            <div>
                <label>kvaliteta:</label>
                <select disabled={this.state.buttonDisable} value='Odaberi sadrzaj' placeholder='Odaberi sadrzaj' onChange={this.handleChange} name='sadrzaj' >
                    <option></option>
                    <option value='DOBRA'>Dobra</option>
                    <option value='NEUTRALNA'>Neutralna</option>
                    <option value='LOSA'>Loša</option>
                </select>
                {message}
            </div>
        )
    }
}

export default QualityFilter;