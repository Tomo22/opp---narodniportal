import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Button from "../Button/Button";
import './Header.css';

class Header extends Component {

  logout = () => {
    fetch('/logout').then(() => {
      this.props.onLogout();
    });
  };

  render() {
      let button;
      let login;
      let register;

      if (!this.props.loggedIn){
          button=<div></div>;
              login=<Link to='/login'>Prijava</Link>;
              register=<Link to='/register'>Registracija</Link>;
      }
      else{
          button=<Button onClick={this.logout}>Odjava</Button>;
          login=<div></div>;
          register=<div></div>;
      }
    return (
      <header className='Header'>
        <Link to='/vijesti'>Naslovnica</Link>
        <Link to='/vijesti/objavi'>Objavi vijest</Link>
          <div style={{flex: 1}}></div>
          {login}
          {register}
          {button}
      </header>
    )
  }
}

export default Header;
