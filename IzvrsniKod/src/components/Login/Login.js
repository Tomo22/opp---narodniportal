import React, {Component} from 'react';
import Form from "../Form/Form";
import Button from "../Button/Button";
import Card from "../Card/Card";
import './Login.css';


class Login extends Component {

  state = {
    username: '',
    password: '',
    error: ''
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
   // const body = `username=${this.state.username}&password=${this.state.password}`;
      const data = {
          nadimak : this.state.username,
          lozinka: this.state.password
      };
    const options = {
      method: 'POST',
      headers: {

          'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    };
    fetch('/login', options)
        .then(response => {
        if (response.status === 400) {
          this.setState({ error: 'Neuspješno! Unijeli ste pogrešne podatke ili ste utišani.' });
        } else {
         this.props.onLogin(this.state.username);
          this.props.history.push('/vijesti')
        }
      })
  };

  render() {
    //console.log(this.props)
    return (
      <Card>
        <Form onSubmit={this.onSubmit}>
          <Form.Row label='Nadimak'>
            <input name='username' onChange={this.handleChange} value={this.state.username}/>
          </Form.Row>
          <br/>
          <Form.Row label='Lozinka'>
            <input name='password' type='password' onChange={this.handleChange} value={this.state.password}/>
          </Form.Row>
          <br/>
          <div className='error'>{this.state.error}</div>
            <br/>
          <Button type='submit'>Prijavi se</Button>
        </Form>
      </Card>
    )
  }
}

export default Login;
