import React from 'react';
import Form from "../Form/Form";
import Button from "../Button/Button";

class MuteUser extends React.Component {

    state= {
        username: '',
        days: '',
        message: '',

    };

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        const user = JSON.parse(localStorage.getItem('user'));

        const options = {
            method: 'POST',
            headers: {

                'Content-Type': 'application/json'
            },
            body: ''
        };

        if(user.admin){
            fetch('/notifications/mute/admin/'+this.state.username+'/'+this.state.days, options)
                .then(response => {
                    if (response.ok) {
                        this.setState({message: 'Korisnik je utišan'})
                    } else {
                        if(this.state.days < "1"){
                            this.setState({ message: 'Neuspješno! Broj dana mora biti veći od 0' });
                        }

                        else this.setState({ message: 'Neuspješno! Korisnik ne postoji, već je utišan ili ga nije moguće utišati' });
                    }
                })
        }
        else {
            fetch('/notifications/mute/editor/'+this.state.username, options)
                .then(response => {
                    if (response.ok) {
                        this.setState({message: 'Korisnik je utišan'})
                    } else {
                        this.setState({ message: 'Neuspješno! Korisnik ne postoji, već je utišan ili ga nije moguće utišati' });
                    }
                })
        }
    };


    render() {
        const user = JSON.parse(localStorage.getItem('user'));
        let admin = <div>
            <input name='username' placeholder=" nadimak" onChange={this.handleChange} value={this.state.username}/>
            <br/>
            <input name='days' type="number"  placeholder=" broj dana" onChange={this.handleChange} value={this.state.days}/>
        </div>

        let urednik =<div>
            <input name='username' placeholder=" nadimak" onChange={this.handleChange} value={this.state.username}/>
        </div>

        return(
            <Form onSubmit={this.onSubmit}>
                {user.admin?admin:urednik}
                <div className='error'>{this.state.message}</div>
                <br/>
                <Button type='submit' disabled={this.state.username.length <=0}>Utišaj</Button>
            </Form>

        )
    }



}

export default MuteUser;

