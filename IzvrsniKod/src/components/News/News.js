import React, {Component} from 'react';
import  {Link} from "react-router-dom";

class News extends Component {

  render() {
    //console.log(this.props.vjest);
    const {headline, id, categoryFirstLevelEvaluationName, categorySecondLevelEvaluationName, categoryThirdLevelEvaluationName, publisher, fakeNews} = this.props.vjest;
    let url = '/vijesti/'+ id;

    let category = <label style={{color: 'blue'}}> Nesvrstano, uđite u vijest i evaluirajte kategoriju </label>

      if(categoryFirstLevelEvaluationName !== null){
          category = <div>
              {categoryFirstLevelEvaluationName} > {categorySecondLevelEvaluationName} > {categoryThirdLevelEvaluationName}
          </div>
      }

    return (
        <div>
            {fakeNews? <label style={{color: 'red'}}>OZNAČENA KAO LAŽ OD STRANE UREDNIKA / ADMINISTRATORA</label>:''}
          <Link to={url} style={{ textDecoration: 'none' }}> <h3>{headline}</h3></Link>
            by {publisher?publisher.nadimak:'anonimni korisnik'}
            <br/>
            {category}
            <hr/>
        </div>
    );
  }
}

export default News;
