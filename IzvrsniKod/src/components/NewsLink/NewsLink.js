import React, { Component } from 'react';
import './NewsLink.css';
import Comment from "../Comment/Comment";
import Button from "../Button/Button";
import EvaluacijaSadrzaja from "../Evaluacija/EvaluacijaSadrzaja";
import EvaluacijaKvalitete from "../Evaluacija/EvaluacijaKvalitete";
import ZatvoriKomDokazi from "../EditorFunctions/ZatvoriKomDokazi";
import OznaciDokazValjan from "../EditorFunctions/OznaciDokazValjan";
import OznaciTrajnoLaz from "../EditorFunctions/OznaciTrajnoLaz";
import EvaluacijaKategorije2 from "../Evaluacija/EvaluacijaKategorije2";





class NewsLink extends Component{

    state = {
        vest:'',
        comments: [],
        regKom: '',
        neregKom:'',
        dokKom:''


    };



    componentDidMount() {
        const id = this.props.match.params.id;
        //console.log(id);


        fetch('/news/'+id)
            .then(data => data.json())
            .then(vest => this.setState( {
                vest: vest,
                comments: vest.comments
            }));

        fetch('/news/comments/'+id)
            .then(data => data.json())
            .then( komentari => this.setState( {
                comments : komentari
            }))
    }

    handleChange =(event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    onKoment = (e) =>{

        e.preventDefault();
        const user = JSON.parse(localStorage.getItem('user'));
        let data = {};
        if(e.target.name=== 'nereg') {
             data= {
                content: this.state.neregKom,
                typeOfComment: 1,
                 news : {
                    id:this.props.match.params.id
                 }
            };
        }

        else if(e.target.name === 'reg'){
             data = {
                content: this.state.regKom,
                typeOfComment: 'REGISTRIRANI',
                commentator: {
                    id: user.id
                },
                news: {
                    id: this.props.match.params.id
                }
            }
        }

        else if (e.target.name === 'dok'){
            data = {
                content: this.state.dokKom,
                typeOfComment: 2,
                commentator: {
                    id: user.id
                },
                news : {
                    id: this.props.match.params.id
                }
            }
        }

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        };

        return fetch('/comments', options)
            .then(response => {
                if(response.ok){

                        window.location.reload();
                }
            })
    };

    render() {

        const logged = JSON.parse(localStorage.getItem('loggedIn'));
        const user  = JSON.parse(localStorage.getItem('user'));

       const {content, id, headline, proof, publisher,closedProofCommentSection,newsProofValid, fakeNews, categoryFirstLevelEvaluationName, categorySecondLevelEvaluationName, categoryThirdLevelEvaluationName, }= this.state.vest;
       const newspublisher = publisher? publisher.nadimak : 'anoniman';

       const isPublic = publisher? publisher.public : false;

        let category = <label style={{color: 'blue'}}> Nesvrstano, molimo Vas da pročitate vijest i evaluirate kategoriju </label>

        if(categoryFirstLevelEvaluationName !== null){
            category = <div>
                {categoryFirstLevelEvaluationName} > {categorySecondLevelEvaluationName} > {categoryThirdLevelEvaluationName}
            </div>
        }


       let javno = <div></div>

        if(isPublic){
            javno=
            <div>
            {publisher.public? <br/>:''}
            <label>{publisher.public? (publisher.ime+' '+publisher.prezime):''}</label>
            {publisher.public? <br/>:''}
            <label>{publisher.public? 'email: '+publisher.email :''} </label>
            <br/>
            </div>
        }

       let uloga=''
        if(publisher){
            if(publisher.admin && !publisher.editor){
                uloga= 'admin'
            }
            else if(publisher.editor && !publisher.admin){
                uloga='urednik'

            }
            else if(publisher.admin && publisher.editor){
                uloga='admin i urednik'
            }
            else {
                uloga='registriran'
            }
        }

        //console.log(this.state.vest)
        //console.log(fakeNews)

       const prolaz = logged && (user.nadimak !== newspublisher) && !fakeNews;
       const prolaz2= logged &&!closedProofCommentSection &&(user.editor)
        const prolaz3= logged &&!newsProofValid &&(user.editor)
        const prolaz4 = logged && !fakeNews &&(user.editor)

        let dokKomeniranje=<div>
            <textarea name='dokKom' disabled={!logged} onChange={this.handleChange} value={this.state.dokKom} />
            <br/>

            <Button  onClick={this.onKoment} name='dok' disabled={!logged || (this.state.dokKom.length <=0)} >Komentiraj </Button>

        </div>

        let message1 =  <div></div>
        let message2 =  <div></div>
        let message3 =  <div></div>

        if (prolaz){
            message1 = <EvaluacijaSadrzaja vijest={this.state.vest}/>;
            message2 = <EvaluacijaKvalitete vijest={this.state.vest}/>;
            message3 = <EvaluacijaKategorije2 vijest={this.state.vest}/>;
        }


        return(


                <div className="row">
                    <div className="NewsLinkLeft">
                        <div className="NewsLinkCard">
                            {fakeNews? <label style={{color: 'red'}}>OZNAČENA LAŽNOM</label>:''}
                            {fakeNews? <br/> :''}

                            {category}
                            <h1>{headline}</h1>
                            <p>{content}</p>
                            <h3>Dokazi autora</h3>
                            <p>{proof}</p>
                            {newsProofValid? <label style={{color: 'red'}}>Dokaz je označen kao valjan</label>: <label style={{color: 'red'}}>Dokaz nije označen kao valjan</label>}
                            <br/>
                            {prolaz3? <OznaciDokazValjan vijest={this.state.vest}/>: ''}
                            <hr/>
                            <h3>Autor</h3>
                            <label>{publisher? publisher.nadimak : 'anoniman'}</label>
                            <br/>
                            <label>{uloga}</label>
                            <label>{javno}</label>
                            <br/>
                            {message1}
                            {message2}
                            {message3}
                            <br/>
                            <hr/>
                            {prolaz2? <ZatvoriKomDokazi vijest={this.state.vest} />: ''}
                            {prolaz4? <OznaciTrajnoLaz vijest={this.state.vest}/>: ''}

                        </div>
                    </div>
                    <div className="NewsLinkRight">
                        <div className="NewsLinkCard">
                            <h3>Registirani komentari</h3>
                            <div>
                            <textarea  name='regKom'  disabled={!logged} onChange={this.handleChange} value={this.state.regKom} />
                            <br/>

                                <Button  onClick={this.onKoment} disabled={!logged || (this.state.regKom.length <=0)} name='reg' >Komentiraj </Button>
                                <hr/>
                            {
                                this.state.comments.map(kom =>
                                    <Comment  key={kom.id} kom={kom} filter='REGISTRIRANI'/>
                                )
                            }
                            </div>

                        </div>
                    </div>
                    <div className="NewsLinkRight">
                        <div className="NewsLinkCard">
                            <h3>Neregistirani komentari</h3>
                            <div>
                                <textarea name='neregKom' disabled={logged} onChange={this.handleChange} value={this.state.neregKom} />
                            <br/>

                                <Button  onClick={this.onKoment} name='nereg' disabled={logged || this.state.neregKom.length <=0} >Komentiraj </Button>
                                <hr/>
                                {
                                    this.state.comments.map(kom =>
                                        <Comment  key={kom.id} kom={kom} filter='NEREGISTRIRANI'/>
                                    )
                                }
                            </div>

                        </div>
                    </div>
                    <div className="NewsLinkRight">
                        <div className="NewsLinkCard">
                            <h3>Komentari s dokazima</h3>
                            <div>
                                {closedProofCommentSection? '': dokKomeniranje}
                                <hr/>
                            {
                                this.state.comments.map(kom =>
                                <Comment key={kom.id} kom={kom} filter='DOKAZI' />)
                            }
                            </div>
                        </div>
                    </div>
                </div>






        )
    }

}

export default NewsLink;
