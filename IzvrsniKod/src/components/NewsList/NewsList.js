import React from 'react';
import News from '../News/News';

import './NewsList.css';
import Button from "../Button/Button";

import ContentFilter from '../Filteri/ContentFilter'
import QualityFilter from '../Filteri/QualityFilter'
import FirstLvlCategory from "../CategoryCreate/FirstLvlCategory";
import SecondLvlCategory from "../CategoryCreate/SecondLvlCategory";
import ThirdLvlCategory from "../CategoryCreate/ThirdLvlCategory";
import MuteUser from '../Mute/MuteUser';
import ProposeAdmin from '../Proposals/ProposeAdmin';
import AbolishAdmin from '../Proposals/AbolishAdmin';
import AbolishEditor from '../Proposals/AbolishEditor';
import Notifications from '../Notifications/Notifications';
import DataPublic from '../DataPublic/DataPublic'

import CategoryFilter from '../Filteri/CategoryFilter'


class NewsList extends React.Component {

  state = {
    news: [],
    filteredNews: [],
    user: {}
  };

  componentDidMount() {
    fetch('/news')
        .then(data => data.json())
        .then(news => {

          this.setState({
            news: news,
            filteredNews: news
          });

            }
        )
    this.handleFilter = this.handleFilter.bind(this);
  }

  handleFilter(filter) {
      if(filter !== ''){
          this.setState({
              filteredNews: this.state.filteredNews.filter((vijest) =>
                  vijest.contentEvaluation === filter || vijest.qualityEvaluation === filter || vijest.categoryFirstLevelEvaluationName === filter
              )
          });
      }
      else{
          this.setState({
              filteredNews: this.state.filteredNews.filter((vijest) =>
                  vijest.categoryFirstLevelEvaluationName === null
              )
          });
      }
  }

  render() {

    const user = JSON.parse(localStorage.getItem('user'))
    let message = '';
    let message2 = <div></div>;
    let message3 = <div></div>;
    let message4 = <div></div>;

    if(user === null){
      message = <div>Registrirajte se kako biste mogli evaluirati vijesti, dobivati rejting i postati urednik!</div>
    }
    else{
      message = <div>
        <h3>Ulogirani korisnik:</h3>
        {user.nadimak}
        <hr/>
        rejting: {user.rating}
        <hr/>
        {user.admin? 'administrator': (user.editor? 'urednik':'registrirani korisnik')}
        <hr/>
          <DataPublic/>
      </div>

      if(user.editor || user.admin){
        message2 =
            <div className="column side">
              <div className="NewsListCard">
                <h2>Opcije {user.admin?'administratora':'urednika'}</h2>
                <hr className="bigHr"/>
                <h3>Utišaj korisnika </h3>
                <MuteUser/>
                <hr className="bigHr2"/>
                  {user.admin? <AbolishEditor/>:''}
                {user.admin? <FirstLvlCategory/>: ''}
                <SecondLvlCategory/>
                <hr/>
               <ThirdLvlCategory/>
                <hr/>
                <Button onClick={() =>{ window.location.reload() }}>Osvježi</Button>
                <hr className="bigHr2"/>
              </div>
            </div>
      }

      if(user.admin){
        message3=
            <div className="NewsListCard">
              <h2>Prijedlozi</h2>
              <hr className="bigHr2"/>
              <ProposeAdmin/>
              <hr/>
              <AbolishAdmin/>
              <hr className="bigHr2"/>
            </div>


        message4=
            <div className="NewsListCard">
              <h2>Obavijesti</h2>
              <hr className="bigHr2"/>
              <Notifications/>
            </div>

      }
    }

    return (
    <div>
      <div className="column side">
        <div className="NewsListCard">
          {message}
          <hr className="bigHr2"/>
        </div>
        {message4}
        {message3}
      </div>
      <div className="column middle">
        <div className="NewsListCard">
          <h4>Filteri:</h4>
          <ContentFilter onFilter={this.handleFilter} />
          <br/>
          <QualityFilter  onFilter={this.handleFilter} />
          <br/>
            <CategoryFilter onFilter={this.handleFilter}/>
            <br/>
          <Button onClick={() =>{ window.location.reload() }}>Poništi filter</Button>
          <hr/>
          <h1>Vijesti</h1>
          {this.state.filteredNews.map(vjest=>
              <News key={vjest.id} vjest={vjest} />)}
        </div>
      </div>
      {message2}
    </div>

    );
  }
}

export default NewsList;
