import React from 'react';
import Button from "../Button/Button";

class Notifications extends React.Component {

    state = {
        username: JSON.parse(localStorage.getItem('user')),
        notifications: []
    };


    componentDidMount() {
        fetch('/notifications/user/'+this.state.username.id)
            .then(data => data.json())
            .then(notif => {

                    this.setState({
                        notifications: notif
                    });
                }
            )
    }


    render() {
        let message = <div></div>

        if(this.state.notifications.length > '0'){
            message =
                <div>
                    {this.state.notifications.map((notif) =>
                       <NotifClass key={notif.id} notif={notif} />)}
                </div>


        }

        return(
            <div>
                {message}
            </div>
        );

    }


}

class NotifClass extends React.Component{

        state = {
            buttonClicked: false,
            typeOfNot: this.props.notif.typeOfNotification,
            NotId: this.props.notif.id,
            message: ''
        }

    handleClickYes = (event) => {
            event.preventDefault();
        this.setState({
            buttonClicked: true,
            message: 'Vaš odgovor je zaprimljen'
        })
        const options = {
            method: 'POST',
            headers: {

                'Content-Type': 'application/json'
            },
            body: ''
        };

        if(this.state.typeOfNot.toString() === '1'){
            fetch('/notifications/voting_new_admin/'+this.state.NotId+'/'+true, options)
        }

        if(this.state.typeOfNot.toString() === '2'){
            fetch('/notifications/voting_remove_admin/'+this.state.NotId+'/'+true, options)
        }

        if(this.state.typeOfNot.toString() === '3'){
            fetch('/notifications/voting_fake_news/'+this.state.NotId+'/'+true, options)
                .then(response => {
                    if (response.status === 400) {
                        this.setState({ message: "Vijest je već proglašena lažnom od strane nekog admina" });
                    } else {
                        this.setState({ message: "Vaš odgovor je zaprimljen"})

                    }
                })

        }

    };

    handleClickNo = (event) => {
        event.preventDefault();
        this.setState({
            buttonClicked: true,
            meesage: "Vaš odgovor je zaprimljen"
        })
        const options = {
            method: 'POST',
            headers: {

                'Content-Type': 'application/json'
            },
            body: ''
        };

        if(this.state.typeOfNot.toString() === '1'){
            fetch('/notifications/voting_new_admin/'+this.state.NotId+'/'+false, options)

        }

        if(this.state.typeOfNot.toString() === '2'){
            fetch('/notifications/voting_remove_admin/'+this.state.NotId+'/'+false, options)
        }

        if(this.state.typeOfNot.toString() === '3'){
            fetch('/notifications/voting_fake_news/'+this.state.NotId+'/'+false, options)
                .then(response => {
                    if (response.status === 400) {
                        this.setState({ message: 'Vijest je već proglašena lažnom od strane nekog admina' });
                    } else {
                        this.setState({ message: "Vaš odgovor je zaprimljen"})

                    }
                })

        }
    };






    render() {
        const {text} = this.props.notif;


        return (
            <div>
                {text}
                <br/>
                <br/>
                <Button disabled={this.state.buttonClicked} onClick={this.handleClickYes}>Da</Button>
                <Button disabled={this.state.buttonClicked} onClick={this.handleClickNo}>Ne</Button>
                <br/>
                {this.state.message}
                <hr/>
            </div>
        );
    }
}



export default Notifications;