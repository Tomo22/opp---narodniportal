import React from 'react';
import Form from "../Form/Form";
import Button from "../Button/Button";

class AbolishAdmin extends React.Component {

    state= {
        username: '',
    };

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    };

    onSubmit = (e) => {
        e.preventDefault();

        const options = {
            method: 'POST',
            headers: {

                'Content-Type': 'application/json'
            },
            body: ''
        };

        fetch('/notifications/remove_admin/'+this.state.username, options)
            .then(response => {
                if (response.ok) {
                    this.setState({message: 'predložili ste da se korisniku '+this.state.username+' ukinu administratorske ovlasti'})
                }
                else {
                    this.setState({ message: 'Neuspješno! Korisnik ne postoji ili nije administrator' });
                }
            })
    };


    render() {


        return(
            <Form onSubmit={this.onSubmit}>
                <h3>Predloži ukidanje administratorskih ovlasti</h3>
                <input name='username' placeholder=" nadimak" onChange={this.handleChange} value={this.state.username}/>
                <div className='error'>{this.state.message}</div>
                <br/>
                <Button type='submit' disabled={this.state.username.length <=0}>Predloži</Button>
            </Form>

        )
    }



}

export default AbolishAdmin;