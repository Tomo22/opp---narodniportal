import React from 'react';
import Form from "../Form/Form";
import Button from "../Button/Button";

class AbolishEditor extends React.Component {

    state= {
        username: '',
        message: '',

    };

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        });
    };

    onSubmit = (e) => {
        e.preventDefault();

        const options = {
            method: 'POST',
            headers: {

                'Content-Type': 'application/json'
            },
            body: ''
        };

        fetch('/users/remove_editor/'+this.state.username, options)
            .then(response => {
                if (response.ok) {
                    this.setState({message: 'Korisniku '+this.state.username+' je ukinuta uloga urednika'})
                }

                else this.setState({ message: 'Neuspješno! Korisnik ne postoji ili nije urednik' });
            })
    };


    render() {

        return(
            <Form onSubmit={this.onSubmit}>
                <h3>Ukloni urednika</h3>
                <input name='username' placeholder=" nadimak" onChange={this.handleChange} value={this.state.username}/>
                <br/>
                <div className='error'>{this.state.message}</div>
                <br/>
                <Button type='submit' disabled={this.state.username.length <=0}>Potvrdi</Button>
                <hr className="bigHr2"/>
            </Form>

        )
    }



}

export default AbolishEditor;
