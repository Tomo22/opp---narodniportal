import React from 'react';
import Card from "../Card/Card";
import Form from "../Form/Form";
import Button from "../Button/Button";

class Publish extends React.Component {

  state = {
    naslov: '',
    tekst: '',
    dokazi: '',
  };

  onSubmit = (e) => {
    //console.log(this.state)
    e.preventDefault();
    let data = {}
    const logged = JSON.parse(localStorage.getItem('loggedIn'));

    if(logged) {
        const korisnik = JSON.parse(localStorage.getItem('user'))
        data = {
            headline: this.state.naslov,
            proof: this.state.dokazi,
            content: this.state.tekst,
            publisher: {
                id: korisnik.id
            }

        };
    }
    else {
        data = {
            headline: this.state.naslov,
            proof: this.state.dokazi,
            content: this.state.tekst
        }
    }

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    };

    return fetch('/news', options)
      .then(response => {
        if (response.ok) {
          this.props.history.push('/vijesti');
        }
      })
  };

  isValid = () => {
    const {naslov, tekst, dokazi} = this.state;
    return naslov.length > 0 && tekst.length > 0 && dokazi.length > 0;
  };


  constructor(props) {
    super(props);
    this.handleChange.bind(this);
  }

  handleChange =(event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
      //console.log(this.props);
      //console.log(JSON.parse(localStorage.getItem('user')))
      const user = JSON.parse(localStorage.getItem('user'))
      //console.log(user.nadimak)

    return (
      <Card title='Nova vijest'>
        <Form onSubmit={this.onSubmit}>
          <Form.Row label='Naslov'>
            <input name='naslov' onChange={this.handleChange} value={this.state.naslov}/>
          </Form.Row>
          <Form.Box label='Tekst'>
            <textarea name='tekst' onChange={this.handleChange} value={this.state.tekst}/>
          </Form.Box>
          <Form.Evidence label='Dokazi'>
            <textarea name='dokazi' onChange={this.handleChange} value={this.state.dokazi}/>
          </Form.Evidence>
          <Button type="submit" disabled={!this.isValid()}>Objavi vijest</Button>
        </Form>
      </Card>
    );
  }
}

export default Publish;
