import React, {Component} from 'react';
import Card from "../Card/Card";
import Form from "../Form/Form";
import Button from "../Button/Button";

class Register extends  Component{

    state= {
        ime:'',
        prezime: '',
        nadimak: '',
        lozinka: '',
        email: '',
        error:'',

    };

    handleChange = (event) =>{
        this.setState({
            [event.target.name] : event.target.value
        })
    };

    onSubmit = (e) =>{
        e.preventDefault();
        const data= {
            ime: this.state.ime,
            prezime: this.state.prezime,
            nadimak: this.state.nadimak,
            lozinka: this.state.lozinka,
            admin: false,
            email: this.state.email,
            editor: false

        };

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        };
        return fetch('/users/', options)
            .then(response => {
                console.log(response);
                if(response.ok){
                    this.props.history.push('/login');
                }
                else{
                    this.setState({ error: 'Postoji korisnik sa navedenim nadimkom ili e-mail adresom' });
                }
            })

    };

    isValid = () => {
      const {ime, prezime, nadimak, lozinka, email} = this.state;
      return ime.length >0 && prezime.length > 0
            && (nadimak.length >0 && nadimak.length<20)
            && lozinka.length >0
          && (email.split('').filter(x=> x=== '@').length===1)
          &&(email.indexOf('.') !==-1 )
    };


    render() {
        return(
            <Card>
                <Form onSubmit={this.onSubmit}>
                    <Form.Row label='Ime'>
                        <input name='ime' onChange={this.handleChange} value={this.state.ime}/>
                    </Form.Row>
                    <br/>
                    <Form.Row label='Prezime'>
                        <input name='prezime' onChange={this.handleChange} value={this.state.prezime}/>
                    </Form.Row>
                    <br/>
                    <Form.Row label='Nadimak'>
                        <input name='nadimak' onChange={this.handleChange} value={this.state.nadimak}/>
                    </Form.Row>
                    <br/>
                    <Form.Row label='Lozinka'>
                        <input type='password' name='lozinka' onChange={this.handleChange} value={this.state.lozinka}/>
                    </Form.Row>
                    <br/>
                    <Form.Row label='E-mail'>
                        <input type='email' name='email' onChange={this.handleChange} value={this.state.email}/>
                    </Form.Row>
                    <br/>
                    <div className='error'>{this.state.error}</div>
                    <br/>
                    <Button type='submit' disabled={!this.isValid()}>Registracija</Button>
                </Form>
            </Card>
        )
    }

}

export default Register;
