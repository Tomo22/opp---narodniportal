﻿19.10.2018. 13:00 - Održan sastanak svih članova tima. Nakon prethodne analize zadatka, na sastanku smo raspravili o mogućim alatima te o podjeli rada.
                    Odlučili smo se sastati opet u ponedjeljak i početi sa izradom dokumentacije.

22.10.2018. 14:00 - Održan sastanak članova tima. Dogovorena je podjela rada među članovima i započeto je pisanje projektne dokumentacije.

28.10.2018. 14:00 - Održan sastanak članova tima. Članovi tima su zajednički radili use case obrasce. 

04.11.2018. 12:00 - Održan sastanak članova tima. Članovi tima su zajednički radili sekvencijske dijagrame.

13.11.2018. 09:00 - Održan sastanak članova tima. Članovi tima su zajednički radili arhitekturu i dizajn sutava.

7.1.2019. 12:00 - Održan sastanak članova tima. Rad na izdradi aplikacije.

17.1.2019. 11:00 - Održan sastanak članova tima. Završavanje projekta.